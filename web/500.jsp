<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width-device-width, initial-scale=1.0">
  <title>ATD Auto Totaal Dienst</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/ATD.css" rel="stylesheet">

</head>
<body>
<%@ include file="header.jsp" %>

<div class="container">
  <div class = "jumbotron text-center">
    <h1>Invallid Request</h1>
    <p>Dit verzoek is niet geldig!<br>
    Probeer het later opnieuw.<br>
    Werkt het dan nog niet neem dan contact op met <a href="mailto:henk@atd.nl">Henk@ATD.nl</a></p>
    <a class="btn-default btn-info btn" href="index.jsp">Terug naar home</a>
  </div>
</div>

<%@ include file="footer.jsp" %>

<script type="js/jquery-2.1.3.min.js"></script>
<script type="js/bootstrap.min.js"></script>

</body>
</html>