<%@ page import="nl.hu.tho4.Utils.ParkeerDAO" %>
<%@ page import="nl.hu.tho4.model.Reservering" %>
<%--
  Created by IntelliJ IDEA.
  User: AvW
  Date: 24-6-2015
  Time: 19:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% // Is ingelogd?
  if (session.getAttribute("username") == null) {
    response.sendRedirect("403.jsp");
  }
%>


<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width-device-width, initial-scale=1.0">
  <title>ATD Auto Totaal Dienst</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<%@ include file="header.jsp" %>
<div class="container">
  <div class="col-md-5">
    <table class="table table-bordered table-hover table-striped">
      <tr><th>ID</th><th>Datum</th><th>Kenteken</th><th>Soort</th></tr>
      <%
        ParkeerDAO pd = new ParkeerDAO();
        for (Reservering r : pd.getAllReservingen()) {
          out.println("<tr><td>" + r.getReserveringsID() + " </td><td>" + r.getDatum() + "</td><td>" + r.getKenteken() + "</td><td>" + pd.getNaamById(r.getReserveringsID()) + "</td></tr>");
        }
      %>

    </table>
  </div>
</div>


<%@ include file="footer.jsp" %>
</body>
</html>
