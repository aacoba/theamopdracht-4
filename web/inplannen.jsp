<%@ page import="nl.hu.tho4.Utils.MonteurDAO" %>
<%@ page import="nl.hu.tho4.Utils.KlantDAO" %>
<%@ page import="nl.hu.tho4.Utils.AutoDAO" %>
<%@ page import="nl.hu.tho4.model.Monteur" %>
<%@ page import="nl.hu.tho4.model.Klant" %>
<%@ page import="nl.hu.tho4.model.Auto" %>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% // Is ingelogd?
	if (session.getAttribute("username") == null) {
		response.sendRedirect("403.jsp");
	}
%>
<html>
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width-device-width, initial-scale=1.0">
	<title>ATD Auto Totaal Dienst</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/ATD.css" rel="stylesheet">

	<script type="js/jquery-2.1.3.min.js"></script>
	<script type="js/bootstrap.min.js"></script>

	<script>
		function removeOptions()
		{
			var selectbox = document.getElementById("autolist");
			var i;
			for(i=selectbox.options.length-1;i>=0;i--)
			{
				selectbox.remove(i);
			}
		}


		function getAutos(klantid) {
			var xmlHttp = new XMLHttpRequest();
			var url = "/getKlantAutos?kid=" + klantid;
			xmlHttp.open("GET", url, false);
			xmlHttp.send(null);
			var response = xmlHttp.responseText;
			var autos = [];


			var lines = response.split('\n');
			for(var i = 0;i < lines.length;i++){
				line = lines[i];
				if (line != "") {
					autos.push(line);
				}
			}
			return autos;
		}

		function updateSelect() {
			var selected = document.getElementById("klantlist").value;
			var autos = getAutos(selected);
			removeOptions();
			var list = document.getElementById("autolist");
			for (var i in autos) {
				list.add(new Option(autos[i], autos[i]));
			}

		}


	</script>

</head>
<body onload="updateSelect()">

<%@ include file="header.jsp" %>

<div class="container">
	<div class = "page-header text-center">
		<h1>Reparaties</h1>
	</div>

	<div class="col-md-3">
		<form method="POST" action="inplannen.do">
			<label>Monteur</label><br>
			<select name="monteur" class="form-control">
				<%
					MonteurDAO mD = new MonteurDAO();
					for (Monteur m: mD.getAllMonteurs()) {
						out.println("<option value=\"" + m.getId() + "\">" + m.getVoornaam() + " " + m.getAchternaam() + "</option>");
					}
				%>
			</select><br><br>
			<label>Klant</label><br>
			<select name="klant" id="klantlist" onchange="updateSelect()" class="form-control">
				<%
					KlantDAO kd = new KlantDAO();
					for(Klant k : kd.getAllKlanten()){
						out.println("<option value=\"" + k.getKlantnr() + "\">"  + k.getInitiaalen() + " " + k.getNaam() + "</option>");
					}
				%>
			</select><br><br>
			<label>Auto</label><br>
			<select name="auto" id="autolist" class="form-control">
			</select><br><br>
			<label>Datum</label><br>
			<input class="form-control" type="date" name="reparatiedag"><br><br>
			<label>Tijd</label><br>
			<input class="form-control" type="time" name="reparatietijd"><br><br>

			<label>Beschrijving</label><br>
			<textarea class="form-control" rows="5" name="desc" placeholder="Beschrijving"></textarea> <br><br>

			<input class="form-control btn-info" type="submit" value="Bevestig reparatie"><br>
		</form>
	</div>
</div>

<%@ include file="footer.jsp" %>


</body>
</html>
