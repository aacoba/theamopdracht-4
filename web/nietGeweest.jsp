<%--
  Created by IntelliJ IDEA.
  User: Nelis
  Date: 24-6-2015
  Time: 12:51
  To change this template use File | Settings | File Templates.
--%>

<%@ page import="nl.hu.tho4.Utils.KlantDAO" %>
<%@ page import="nl.hu.tho4.model.Klant" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% // Is ingelogd?
  if (session.getAttribute("username") == null) {
    response.sendRedirect("403.jsp");
  }
%>


<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width-device-width, initial-scale=1.0">
  <title>ATD Auto Totaal Dienst</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<%@ include file="header.jsp" %>
<div class="container">
  <div class = "page-header text-center">
    <h1>Klanten die lang niet langs zijn geweest</h1>
  </div>
  <div>
    <table class="table table-striped table-bordered table-responsive table-hover">
      <tr><th>KlantNummer</th><th>Initialen</th><th>Naam</th><th>Detail</th></tr>
      <%
        KlantDAO kd = new KlantDAO();

        ArrayList<Klant> list;
         list = kd.getLangNietGeweest();

        for (Klant k : list) {
          out.print("<tr>");
          out.print("<td>" + k.getKlantnr() + "</td>");
          out.print("<td>" + k.getInitiaalen() + "</td>");
          out.print("<td>" + k.getNaam() + "</td>");
          out.print("<td><a href=\"klant.jsp?kid=" + k.getKlantnr() + "\"class=\"btn btn-info btn-block\">Info</a></td>");
          out.println("</tr>");
        }
      %>

    </table>
    <div class="col-md-3">
      <a href="/klantOverzicht.jsp" class="btn btn-info text-center btn-block">Klanten Overzicht</a>
    </div>

  </div>
</div>
<%@ include file="footer.jsp" %>
<script type="js/jquery-2.1.3.min.js"></script>
<script type="js/bootstrap.min.js"></script>
</body>
</html>
