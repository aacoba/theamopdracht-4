<%--
  Created by IntelliJ IDEA.
  User: Gebruiker
  Date: 25-6-2015
  Time: 1:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="nl.hu.tho4.Utils.ParkeerDAO" %>
<%@ page import="nl.hu.tho4.Utils.Utils" %>

<% // Is ingelogd?
  if (session.getAttribute("username") == null) {
    response.sendRedirect("403.jsp");
  }

  // Check of er een parameter bij zit
  int soortID = 0;
  String rawSoortID = request.getParameter("sid");
  if (rawSoortID == null) {
    response.sendRedirect("500.jsp");
  } else {
    try {
      soortID = Integer.valueOf(rawSoortID);

    } catch (NumberFormatException e) {
      response.sendRedirect("500.jsp");
    }
  }
  Utils u = new Utils();
  ParkeerDAO bd = new ParkeerDAO();
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width-device-width, initial-scale=1.0">
  <title>ATD Auto Totaal Dienst</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/bootstrap-formhelpers.css" rel="stylesheet" media="screen">
  <link href="css/ATD.css" rel="stylesheet">
  <title>ATD Auto Totaal Dienst</title>
</head>
<body>
<%@ include file="header.jsp" %>
<div class="container">
  <div class = "page-header text-center">
    <h1>Overzicht Brandstof</h1>
  </div>
  <div class="col-md-3">
    <form method="POST" action="/setPrijs.do">
      <input type="number" class="form-control" name="prijs" value="<%out.print(bd.getPrijsByID(soortID));%>">
      <input type="hidden" name="id" value="<%out.print(soortID);%>">
      <input class="form-control btn-info btn-block" type="submit" value="Update tarief prijs">
    </form>
  </div>
</div>


<%@ include file="footer.jsp" %>
<script src="js/jquery-2.1.3.min.js"></script>
<script src="js/bootstrap-formhelpers.js"></script>

</body>
</html>
