<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	if (session.getAttribute("username") != null) {
		response.sendRedirect("index.jsp");
	}

%>
<html>
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width-device-width, initial-scale=1.0">
	<title>ATD Auto Totaal Dienst</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">

</head>
<body>

<%@ include file="header.jsp" %>

<div class="container">
	<div class="col-md-offset-3 col-md-5 text-center">
		<%
			if (request.getParameter("msg") != null) {
				if (request.getParameter("msg").equals("invallid")) {
					out.println("<div class=\"alert alert-danger\" role=\"alert\"><b>Sorry! je login is incorrect :(</b></div>");
			}

		} %>

		<form action="login.do" method="POST">
			<div class="form-group">
				<label for="loginUsername">Gebruikersnaam</label>
				<input name="username" type="text" class="form-control" id="loginUsername" placeholder="Gebruikersnaam">
			</div>
			<div class="form-group">
				<label for="loginPassword">Password</label>
				<input name="password" type="password" class="form-control" id="loginPassword" placeholder="Password">
			</div>
			<button type="submit" class="btn btn-default">Login!</button>
		</form>
	</div>
</div>

<%@ include file="footer.jsp" %>

<script type="js/jquery-2.1.3.min.js"></script>
<script type="js/bootstrap.min.js"></script>


</body>
</html>