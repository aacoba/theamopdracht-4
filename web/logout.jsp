<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  session.invalidate();
  request.getSession();
%>
<html>
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width-device-width, initial-scale=1.0">
  <title>ATD Auto Totaal Dienst</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/ATD.css" rel="stylesheet">

</head>
<body>


<div class="container">
  <div class="jumbotron">
    <h1>U bent succesvol uitgelogd!</h1>
    <p><a class="btn btn-primary btn-lg" href="index.jsp" role="button">Homepage</a></p>
  </div>
</div>

<%@ include file="footer.jsp" %>

<script type="js/jquery-2.1.3.min.js"></script>
<script type="js/bootstrap.min.js"></script>


</body>
</html>