<%--
  Created by IntelliJ IDEA.
  User: Gebruiker
  Date: 25-6-2015
  Time: 1:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="nl.hu.tho4.Utils.ParkeerDAO" %>
<%@ page import="nl.hu.tho4.Utils.Utils" %>
<%@ page import="nl.hu.tho4.model.Parkeerprijs" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% // Is ingelogd?
  if (session.getAttribute("username") == null) {
    response.sendRedirect("403.jsp");
  }
%>


<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width-device-width, initial-scale=1.0">
  <title>ATD Auto Totaal Dienst</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<%@ include file="header.jsp" %>
<div class="container">
  <div class = "page-header text-center">
    <h1>Tarief overzicht</h1>
  </div>
  <div>
    <table class="table table-striped table-bordered table-responsive table-hover">
      <tr><th>ID</th><th>Soort</th><th>Prijs</th><th>Info</th></tr>
      <%
        ParkeerDAO pd = new ParkeerDAO();
        Utils u = new Utils();
        for (Parkeerprijs t : pd.getAllParkeertariefen()) {
          out.print("<tr>");
          out.print("<td>" + t.getSoortid() + "</td>");
          out.print("<td>" + t.getSoort() + "</td>");
          out.print("<td>" + u.curFormat(t.getPrijs()) + "</td>");
          out.print("<td><a href=\"tarief.jsp?sid=" + t.getSoortid() + "\"class=\"btn btn-info btn-block\">Aanpassen</a></td>");
          out.println("</tr>");
        }
      %>
    </table>
  </div>
</div>
<%@ include file="footer.jsp" %>
<script type="js/jquery-2.1.3.min.js"></script>
<script type="js/bootstrap.min.js"></script>
</body>
</html>
