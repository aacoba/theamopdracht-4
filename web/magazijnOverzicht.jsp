<%@ page import="nl.hu.tho4.Utils.ArtikelDAO" %>
<%@ page import="nl.hu.tho4.model.Artikel" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% // Is ingelogd?
  if (session.getAttribute("username") == null) {
    response.sendRedirect("403.jsp");
  }
%>


<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width-device-width, initial-scale=1.0">
  <title>ATD Auto Totaal Dienst</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<%@ include file="header.jsp" %>
<div class="container">
  <div class = "page-header text-center">
    <h1>Overzicht Artikelen</h1>
  </div>
  <div>
    <div class="col-md-5">
      <form action="magazijnOverzicht.jsp" method="GET" class="form-inline">
        <div class="form-group">
          <label class="sr-only" for="searchQ">Zoekopdracht</label>
          <div class="input-group">
            <div class="input-group-addon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
            <input type="text" class="form-control" name="q" id="searchQ" placeholder="Zoeken">
          </div>
        </div>
        <button type="submit" class="btn btn-primary">Zoeken</button>
      </form>
    </div>
      <br><br>
    <table class="table table-striped table-bordered table-responsive table-hover">
      <tr><th>Naam</th><th>EAN</th><th>Hoeveelheid</th><th>Gewenste Hoeveelheid</th><th>Inkoopprijs</th><th>Adviesprijs</th></tr>
      <%
        ArtikelDAO ad = new ArtikelDAO();


        ArrayList<Artikel> list;
        if (request.getParameter("q") != null && !request.getParameter("q").equals("")) {
          list = ad.searchArtikelen(request.getParameter("q"));
        } else {
          list = ad.getAllArtikelen();
        }

        for (Artikel a : list) {
          out.print("<tr>");
          out.print("<td>" + a.getNaam() + "</td>");
          out.print("<td>" + a.getEuropeesArtikelNummer() + "</td>");
          out.print("<td>" + a.getHoeveelheid() + "</td>");
          out.print("<td>" + a.getGewensteHoeveelheid() + "</td>");
          out.print("<td>" + a.getInkoopPrijs() + "</td>");
          out.print("<td>" + a.getAdviesPrijsEuro() + "</td>");
          out.println("</tr>");
        }
      %>
    </table>
  </div>
  <a href="lageVoorraad.jsp?" class="btn btn-info text-center btn-block">Lage voorraad controleren</a>
</div>
<%@ include file="footer.jsp" %>
<script type="js/jquery-2.1.3.min.js"></script>
<script type="js/bootstrap.min.js"></script>
</body>
</html>

