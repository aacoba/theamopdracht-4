<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width-device-width, initial-scale=1.0">
	<title>ATD Auto Totaal Dienst test</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">

</head>
<body>



<%@ include file="header.jsp" %>

<div class="container">
	<div class = "page-header text-center">
		<h1>Over ons</h1>
	</div>

	<div>
		Wij zijn AutototaalDienst en bij ons kunt u terecht voor al uw reparaties en onderhoud.
		Ook bent u van harte welkom in onze parkeer garage en kunt u bij ons tanken.
		Wij streven naar een goede klanten relatie en daarom kunt u op deze site een reparatie aanvragen. Dan plannen wij u zo snel mogelijk in.

		Met vriendelijke groet,

		Henk
	</div>
</div>

<%@ include file="footer.jsp" %>

<script type="js/jquery-2.1.3.min.js"></script>
<script type="js/bootstrap.min.js"></script>


</body>
</html>