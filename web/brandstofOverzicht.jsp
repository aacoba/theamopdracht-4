<%--
  Created by IntelliJ IDEA.
  User: Nelis
  Date: 27-5-2015
  Time: 14:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="nl.hu.tho4.Utils.BrandstofDAO" %>
<%@ page import="nl.hu.tho4.model.Brandstof" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% // Is ingelogd?
  if (session.getAttribute("username") == null) {
    response.sendRedirect("403.jsp");
  }
%>


<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width-device-width, initial-scale=1.0">
  <title>ATD Auto Totaal Dienst</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<%@ include file="header.jsp" %>
<div class="container">
  <div class = "page-header text-center">
    <h1>Overzicht Brandstof</h1>
  </div>
  <div>
    <table class="table table-striped table-bordered table-responsive table-hover">
      <tr><th>Brandstof ID</th><th>Naam</th><th>Prijs per Liter</th><th>Hoeveelheid</th><th>Gewenste hoeveelheid</th><th>Info</th></tr>
      <%
        BrandstofDAO bd = new BrandstofDAO();
        for (Brandstof b : bd.getAllBrandstof()) {
          out.print("<tr>");
          out.print("<td>" + b.getBrandstofID() + "</td>");
          out.print("<td>" + b.getNaam() + "</td>");
          out.print("<td>" + b.getPrijsPerLiter() + "</td>");
          out.print("<td>" + b.getHoeveelheid() + "</td>");
          out.print("<td>" + b.getGewensteHoeveelheid() + "</td>");
          out.print("<td><a href=\"brandstof.jsp?bid=" + b.getBrandstofID() + "\"class=\"btn btn-info btn-block\">Info</a></td>");
          out.println("</tr>");
        }
      %>
    </table>
  </div>
</div>
<%@ include file="footer.jsp" %>
<script type="js/jquery-2.1.3.min.js"></script>
<script type="js/bootstrap.min.js"></script>
</body>
</html>
