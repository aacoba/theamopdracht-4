<%@ page import="nl.hu.tho4.Utils.ArtikelDAO" %>
<%@ page import="nl.hu.tho4.Utils.KlantDAO" %>
<%@ page import="nl.hu.tho4.Utils.KlusDAO" %>
<%@ page import="nl.hu.tho4.Utils.Utils" %>
<%@ page import="nl.hu.tho4.model.Artikel" %>
<%@ page import="nl.hu.tho4.model.Klant" %>
<%@ page import="nl.hu.tho4.model.Klus" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% // Is ingelogd?
    if (session.getAttribute("username") == null) {
        response.sendRedirect("403.jsp");
    }

    // Check of er een parameter bij zit
    int klusID = 0;
    String rawKlusID = request.getParameter("kid");
    if (rawKlusID == null) {
        response.sendRedirect("500.jsp");
    } else {
        try {
            klusID = Integer.valueOf(rawKlusID);

        } catch (NumberFormatException e) {
            response.sendRedirect("500.jsp");
        }
    }
    Utils u = new Utils();
    KlusDAO kd = new KlusDAO();
    String status = kd.getStatus(klusID);
    Klus k = kd.getKlusById(klusID);
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width-device-width, initial-scale=1.0">
    <title>ATD Auto Totaal Dienst</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-formhelpers.css" rel="stylesheet" media="screen">
    <link href="css/ATD.css" rel="stylesheet">
    <title>ATD Auto Totaal Dienst</title>
</head>
<body>
<%@ include file="header.jsp" %>
<div class="container">
    <div class="page-header text-center">
        <h1>Reparaties</h1>
    </div>

    <div class="col-md-3">
        <h4>Artikelen toevoegen</h4>


        <form action="addGebruiktArtikel.do" method="POST">
            <select class="form-control" name="ean">
                <%
                    ArtikelDAO ad = new ArtikelDAO();
                    for (Artikel a : ad.getAllArtikelen()) {
                        out.print("<option value=\"" + a.getEuropeesArtikelNummer() + "\">" + a.getEuropeesArtikelNummer() + " " + a.getNaam() + "</option>");
                    }
                %>
            </select>
            <input type="hidden" name="klusid" value="<%out.print(klusID);%>">
            <%
                if (kd.getStatus(klusID).equals("Betaald")) {
                    out.print("<button type=\"submit\" class=\"btn btn-info btn-block\" disabled=\"disabled\">Voeg Artikel Toe</button>");
                } else {
                    out.print("<button type=\"submit\" class=\"btn btn-info btn-block\">Voeg Artikel Toe</button>");
                }
            %>
        </form>
    </div>

    <div class="col-md-3">
        <h4>Klus Status</h4>

        <form method="POST" action="klusStatus.do">
            <select class="form-control" name="status">
                <option value="In Wacht"<%out.print(u.klusStatusSelector("In Wacht", status));%>>In Wacht</option>
                <option value="Begonnen"<%out.print(u.klusStatusSelector("Begonnen", status));%>>Begonnen</option>
                <option value="Klaar"<%out.print(u.klusStatusSelector("Klaar", status));%>>Klaar</option>
                <option value="Betaald"<%out.print(u.klusStatusSelector("Betaald", status));%>>Betaald</option>
            </select>
            <input type="hidden" name="klusID" value="<%out.print(klusID);%>">
            <input class="form-control btn-info btn-block" type="submit" value="Update Status">
        </form>
        <br>

        <form method="POST" action="/setWerktijd.do">
            <input type="text" class="bfh-number form-control" name="tijd"
                   value="<%out.print(kd.getWerktijdById(klusID));%>">
            <input type="hidden" name="klusID" value="<%out.print(klusID);%>">
            <% if (kd.getStatus(klusID).equals("Betaald")) {
                out.print("<button type=\"submit\" class=\"btn btn-info btn-block\" disabled=\"disabled\">Update werktijd</button>");
            } else {
                out.print("<button type=\"submit\" class=\"btn btn-info btn-block\">Update werktijd</button>");
            }
            %>

        </form>


        <a href="afrekenen.jsp?kid=<%out.print(klusID);%>" class="btn btn-info text-center btn-block">Afrekenen</a>
    </div>
    <div class="col-md-6">
        <h4>Klus Info</h4>
        <%
            KlantDAO kld = new KlantDAO();
            Klant kl = kld.getKlantByKlusID(klusID);

        %>
        <table class="table table-bordered table-striped">
            <tr>
                <td>Kenteken</td>
                <td><% out.print(k.getKenteken());%></td>
            </tr>
            <tr>
                <td>Klant</td>
                <td><% out.print(kl.getInitiaalen() + " " + kl.getNaam());%></td>
            </tr>
        </table>
        <h4>Beschrijving aanpassen</h4>
        <form method="POST" action="updateBeschrijving.do">
            <textarea name="desc" class="form-control" placeholder="beschrijving"><% out.print(k.getBeschrijving()); %></textarea>
            <input type="hidden" name="klusID" value="<%out.print(klusID);%>">
            <input class="form-control btn-info btn-block" type="submit" value="Update Beschrijving">
        </form>

        <h4>Gebruikte Artikelen</h4>
        <table class="table table-striped table-bordered table-responsive table-hover">
            <tr>
                <th>Naam</th>
                <th>EAN</th>
                <th>Prijs</th>
            </tr>
            <%
                for (Artikel a : kd.getGebruikteArtikelen(klusID)) {
                    out.print("<tr>");
                    out.print("<td>" + a.getNaam() + "</td>");
                    out.print("<td>" + a.getEuropeesArtikelNummer() + "</td>");
                    out.print("<td>" + a.getAdviesPrijsEuro() + "</td>");
                    out.println("</tr>");
                }
            %>
        </table>
    </div>

</div>

<%@ include file="footer.jsp" %>
<script src="js/jquery-2.1.3.min.js"></script>
<script src="js/bootstrap-formhelpers.js"></script>

</body>
</html>
