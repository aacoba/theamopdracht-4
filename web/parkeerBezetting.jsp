<%@ page import="nl.hu.tho4.model.ParkeerBezetting" %>
<%@ page import="nl.hu.tho4.Utils.ParkeerDAO" %>
<%--
  Created by IntelliJ IDEA.
  User: AvW
  Date: 24-6-2015
  Time: 19:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% // Is ingelogd?
  if (session.getAttribute("username") == null) {
    response.sendRedirect("403.jsp");
  }
%>


<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width-device-width, initial-scale=1.0">
  <title>ATD Auto Totaal Dienst</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <script>
    function updateDate(nwDate) {
      $('#dateField').val(nwDate);
    }
  </script>
</head>
<body>
<%@ include file="header.jsp" %>

<div class="container">

  <div class="page-header text-center">
    <h1>Parkeerbezetting</h1>
  </div>

  <div class="col-md-4 table-responsive">
    <table class="table table-striped table-bordered table-hover">
      <tr><th>Datum</th><th>Bezetting</th><th>Aanpassen</th></tr>
      <%
        ParkeerDAO pd = new ParkeerDAO();

        for (ParkeerBezetting pk : pd.getAllBezetting()) {
          out.println("<tr><td>" + pk.getDatum() + "</td><td>" + pk.getBezetting() + "</td><td><button class='btn btn-default' type='button' onClick='updateDate(\"" + pk.getDatum() + "\")'>Aanpassen</button></td></tr>");
        }

      %>
    </table>
  </div>
  <div class="col-md-3">
    <form action="/updateBezetting.do" method="POST">
      <input class="form-control" id="dateField" type="date" name="datum">
      <input class="form-control" type="number" name="bezetting">
      <button class="btn btn-info btn-block" type="submit">Bezetting doorgeven</button>
    </form>
  </div>
</div>

<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

</body>
</html>
