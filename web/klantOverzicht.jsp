<%--
  Created by IntelliJ IDEA.
  User: Nelis
  Date: 27-5-2015
  Time: 13:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="nl.hu.tho4.Utils.KlantDAO" %>
<%@ page import="nl.hu.tho4.model.Klant" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% // Is ingelogd?
  if (session.getAttribute("username") == null) {
    response.sendRedirect("403.jsp");
  }
%>


<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width-device-width, initial-scale=1.0">
  <title>ATD Auto Totaal Dienst</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<%@ include file="header.jsp" %>
<div class="container">
  <div class = "page-header text-center">
    <h1>Klanten overzicht</h1>
  </div>
  <div class="col-md-5">
    <form action="klantOverzicht.jsp" method="GET" class="form-inline">
      <div class="form-group">
        <label class="sr-only" for="searchQ">Zoekopdracht</label>
        <div class="input-group">
          <div class="input-group-addon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
          <input type="text" class="form-control" name="q" id="searchQ" placeholder="Zoeken">
        </div>
      </div>
      <button type="submit" class="btn btn-primary">Zoeken</button>
    </form>
  </div>
  <br><br>
  <div>
    <table class="table table-striped table-bordered table-responsive table-hover">
      <tr><th>KlantNummer</th><th>Initialen</th><th>Naam</th><th>Adres</th><th>Postcode</th><th>EmailAdres</th><th>Detail</th></tr>
      <%
        KlantDAO kd = new KlantDAO();

        ArrayList<Klant> list;
        if (request.getParameter("q") != null && !request.getParameter("q").equals("")) {
          list = kd.searchKlant(request.getParameter("q"));
        } else {
          list = kd.getAllKlanten();
        }

        for (Klant k : list) {
          out.print("<tr>");
          out.print("<td>" + k.getKlantnr() + "</td>");
          out.print("<td>" + k.getInitiaalen() + "</td>");
          out.print("<td>" + k.getNaam() + "</td>");
          out.print("<td>" + k.getAdres() + "</td>");
          out.print("<td>" + k.getPostcode() + "</td>");
          out.print("<td>" + k.getEmail() + "</td>");
          out.print("<td><a href=\"klant.jsp?kid=" + k.getKlantnr() + "\"class=\"btn btn-info btn-block\">Info</a></td>");
          out.println("</tr>");
        }
      %>

    </table>
    <div class="col-md-3">
      <a href="verlopenAPK.jsp" class="btn btn-info text-center btn-block">APK Controleren</a>
      <a href="nietGeweest.jsp" class="btn btn-info text-center btn-block">Lang niet geweest</a>
    </div>

  </div>
</div>
<%@ include file="footer.jsp" %>
<script type="js/jquery-2.1.3.min.js"></script>
<script type="js/bootstrap.min.js"></script>
</body>
</html>

