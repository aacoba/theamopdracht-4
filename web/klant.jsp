<%@ page import="nl.hu.tho4.Utils.KlantDAO" %>
<%@ page import="nl.hu.tho4.Utils.KlusDAO" %>
<%@ page import="nl.hu.tho4.Utils.AutoDAO" %>
<%@ page import="nl.hu.tho4.Utils.Utils" %>
<%@ page import="nl.hu.tho4.model.Klant" %>
<%@ page import="nl.hu.tho4.model.Klus" %>
<%@ page import="nl.hu.tho4.model.Auto" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="nl.hu.tho4.Utils.MailUtils" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% // Is ingelogd?
  if (session.getAttribute("username") == null) {
    response.sendRedirect("403.jsp");
  }

  // Check of er een parameter bij zit
  int klantID = 0;
  String rawKlantID = request.getParameter("kid");
  if (rawKlantID == null) {
    response.sendRedirect("500.jsp");
  } else {
    try {
      klantID = Integer.valueOf(rawKlantID);

    } catch (NumberFormatException e) {
      response.sendRedirect("500.jsp");
    }
  }
  Utils u = new Utils();
  Klant k = new KlantDAO().getKlantByID(klantID);
%>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width-device-width, initial-scale=1.0">
  <title>ATD Auto Totaal Dienst</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/bootstrap-formhelpers.css" rel="stylesheet" media="screen">
  <link href="css/ATD.css" rel="stylesheet">
  <title>ATD Auto Totaal Dienst</title>

  <script src="ckeditor/ckeditor.js"></script>
</head>
<body>
<%@ include file="header.jsp" %>
<div class="container">
  <div class = "page-header text-center">
    <h1>Klant: <% out.print(k.getInitiaalen() + " " + k.getNaam()); %> </h1>
  </div>

  <div class="col-md-3">
    <h3>Gegevens aanpassen</h3>
    <form action="updateKlant.do" method="POST">
      <div class="form-group">
        <label for="klantId">Klant Nummer</label>
        <input type="text" class="form-control" id="klantId" name="klantNr" placeholder="Klant Nr" value="<%out.print(k.getKlantnr());%>" readonly>
      </div>
      <div class="form-group">
        <label for="klantInitialen">Initiaalen</label>
        <input type="text" class="form-control" id="klantInitialen" name="initialen" placeholder="Initialen" value="<%out.print(k.getInitiaalen());%>">
      </div>
      <div class="form-group">
        <label for="klantNaam">Naam</label>
        <input type="text" class="form-control" id="klantNaam" name="naam" placeholder="Naam" value="<%out.print(k.getNaam());%>">
      </div>
      <div class="form-group">
        <label for="klantAdres">Adres</label>
        <input type="text" class="form-control" id="klantAdres" name="adres" placeholder="Adres" value="<%out.print(k.getAdres());%>">
      </div>
      <div class="form-group">
        <label for="klantPostcode">Postcode</label>
        <input type="text" class="form-control" id="klantPostcode" name="postcode" placeholder="Postcode" value="<%out.print(k.getPostcode());%>">
      </div>
      <div class="form-group">
        <label for="klantEmail">Email</label>
        <input type="text" class="form-control" id="klantEmail" name="email" placeholder="Email" value="<%out.print(k.getEmail());%>">
      </div>
      <button type="submit" class="btn btn-default">Bewerken</button>
    </form>
  </div>
  <div class="col-md-6">
    <h3>Bijhorende Klussen</h3>
      <%
        KlusDAO kd = new KlusDAO();
        out.print(kd.getHTMLTable(kd.getKlussenByKlant(klantID)));

      %>
  </div>
  <div class="col-md-3">
    <h3>Herinneringsbrief</h3>
    <h6>Laatste Herinnering: <% out.print(k.getLaatsteHerinnering().toString()); %></h6>
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#briefModal">
      Herinnering Opstellen
    </button>
  </div>

</div>

<div class=".col-md-4 container">
  <h3>Auto's</h3>
  <table class="table table-striped table-bordered table-responsive table-hover">
    <tr><th>Klant Nummer</th><th>Kenteken</th><th>Laatste APK</th></tr>
  <%
    AutoDAO ad = new AutoDAO();

    ArrayList<Auto> list;

    list = ad.getCustomerCarsByID(klantID);

    for (Auto a : list) {
      out.print("<tr>");
      out.print("<td>" + a.getKlantNr()+ "</td>");
      out.print("<td>" + a.getKenteken() + "</td>");
      out.print("<td>" + a.getLaatsteAPK() + "</td>");
      out.println("</tr>");
    }
  %>
  </table>
</div>


<!-- Modal -->
<div class="modal fade" id="briefModal" tabindex="-1" role="dialog" aria-labelledby="briefModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="briefModalLabel">Herinnerings Email Opstellen</h4>
      </div>
      <div class="modal-body">
        <form id="mailform" action="sendMail.do" method="POST">
          <div class="form-group">
            <label for="messageReceiver">Ontvanger</label>
            <input type="text" class="form-control" id="messageReceiver" placeholder="Ontvanger" name="to" value="<%out.print(k.getEmail());%>" readonly>
          </div>
          <div class="form-group">
            <label for="messageSubject">Onderwerp</label>
            <input type="text" class="form-control" id="messageSubject" placeholder="Onderwerp" name="subject" value="Autotaaldiensten Herinnering">
          </div>
          <div class="form-group">
            <%--<label for="messageContent">Inhoud</label>--%>
            <textarea id="mailbody" class="form-control" id="messageContent" placeholder="Inhoud" name="body"><%out.print(new MailUtils().getHerrinneringBody(k));%></textarea>
          </div>
          <div class="form-control">
            <input type="hidden" name="nextRedir" value="/klant.jsp?kid=<%out.print(klantID);%>">
          </div>
          <%--<button type="submit" class="btn btn-default">Submit</button>--%>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
        <button type="button" class="btn btn-primary" id="mailsendButton">Versturen</button>
      </div>
    </div>
  </div>
</div>

<%@ include file="footer.jsp" %>
<script src="js/jquery-2.1.3.min.js"></script>
<script src="js/bootstrap-formhelpers.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script>
  CKEDITOR.replace('mailbody')

  $(function() {
    $("#mailsendButton").click( function()
            {
              $("#mailform").submit()
            }
    );
  });


</script>

</body>
</html>
