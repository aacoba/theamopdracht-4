<%@ page import="nl.hu.tho4.Utils.KlantDAO" %>
<%@ page import="nl.hu.tho4.Utils.KlusDAO" %>
<%@ page import="nl.hu.tho4.Utils.MailUtils" %>
<%@ page import="nl.hu.tho4.Utils.Utils" %>
<%@ page import="nl.hu.tho4.model.Artikel" %>
<%@ page import="nl.hu.tho4.model.Klant" %>

<%--
  Created by IntelliJ IDEA.
  User: AvW
  Date: 20-5-2015
  Time: 15:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%

    // Is ingelogd?
    if (session.getAttribute("username") == null) {
        response.sendRedirect("403.jsp");
    }

    // Check of er een parameter bij zit
    int klusID = 0;
    String rawKlusID = request.getParameter("kid");
    if (rawKlusID == null) {
        response.sendRedirect("500.jsp");
    } else {
        try {
            klusID = Integer.valueOf(rawKlusID);

        } catch (NumberFormatException e) {
            response.sendRedirect("500.jsp");
        }
    }
    KlusDAO kd = new KlusDAO();
    KlantDAO kld = new KlantDAO();
    Klant klant = kld.getKlantByKlusID(klusID);
%>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width-device-width, initial-scale=1.0">
    <title>ATD Auto Totaal Dienst</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<%@ include file="header.jsp" %>
<div class="container">
    <div class="page-header text-center">
        <h1>Afrekenen</h1>
    </div>
    <div>
        <h1>Gebruikte Artikelen</h1>
        <table class="table table-striped table-bordered table-responsive table-hover">
            <tr>
                <th>Naam</th>
                <th>EAN</th>
                <th>Prijs</th>
            </tr>
            <%

                Utils u = new Utils();
                for (Artikel a : kd.getGebruikteArtikelen(klusID)) {
                    out.print("<tr>");
                    out.print("<td>" + a.getNaam() + "</td>");
                    out.print("<td>" + a.getEuropeesArtikelNummer() + "</td>");
                    out.print("<td>" + a.getAdviesPrijsEuro() + "</td>");
                    out.println("</tr>");
                }
            %>
        </table>

        <div class="col-md-5">
            <form method="POST" action="/setKorting.do">
                <input type="hidden" name="klusID" value="<%out.print(klusID);%>">

                <table class="table table-striped table-bordered table-responsive table-hover">
                    <tr>
                        <td>Aantal Werkuren</td>
                        <td><% out.print(u.formatUren(kd.getWerktijdById(klusID))); %></td>
                    </tr>
                    <tr>
                        <td>Totaal</td>
                        <td><% out.println(u.curFormat(kd.getTotaal(klusID)));%></td>
                    </tr>
                    <tr>
                        <td>Korting</td>
                        <td><input type="text" class="bfh-number form-control" name="korting"
                                   value="<%out.print(kd.getKortingById(klusID));%>"></td>
                    </tr>
                    <tr>
                        <td>Subtotaal</td>
                        <td><% out.println(u.curFormat(kd.getTotaalPrijs(klusID)));%></td>
                    </tr>
                </table>
                <input class="form-control btn-info" type="submit" value="Voeg korting toe">
            </form>
        </div>
        <div class="col-md-3">
            <form method="POST" action="/klusStatus.do">
                <input type="hidden" name="klusID" value="<%out.print(klusID);%>">
                <input type="hidden" name="status" value="Betaald">
                <%
                    if (kd.getStatus(klusID).equals("Betaald")) {
                        out.print("<button type=\"submit\" class=\"btn btn-lg btn-danger\" disabled=\"disabled\">Is afgerekend</button>");
                    } else {
                        out.print("<button type=\"submit\" class=\"btn btn-lg btn-danger\">Is afgerekend</button>");
                    }
                %>

            </form>
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#briefModal">
                Maak Factuur
            </button>

        </div>



    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="briefModal" tabindex="-1" role="dialog" aria-labelledby="briefModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="briefModalLabel">Factuur Email Opstellen</h4>
            </div>
            <div class="modal-body">
                <form id="mailform" action="sendMail.do" method="POST">
                    <div class="form-group">
                        <label for="messageReceiver">Ontvanger</label>
                        <input type="text" class="form-control" id="messageReceiver" placeholder="Ontvanger" name="to" value="<%out.print(klant.getEmail());%>" readonly>
                    </div>
                    <div class="form-group">
                        <label for="messageSubject">Onderwerp</label>
                        <input type="text" class="form-control" id="messageSubject" placeholder="Onderwerp" name="subject" value="Autotaaldiensten Factuur">
                    </div>
                    <div class="form-group">
                        <%--<label for="messageContent">Inhoud</label>--%>
                        <textarea id="mailbody" class="form-control" id="messageContent" placeholder="Inhoud" name="body"><%out.print(new MailUtils().getFactuurBody(kd.getKlusById(klusID)));%></textarea>
                    </div>
                    <div class="form-control">
                        <input type="hidden" name="nextRedir" value="/afrekenen.jsp?kid=<%out.print(klusID);%>">
                    </div>
                    <%--<button type="submit" class="btn btn-default">Submit</button>--%>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
                <button type="button" class="btn btn-primary" id="mailsendButton">Versturen</button>
            </div>
        </div>
    </div>
</div>


<script src="js/jquery-2.1.3.min.js"></script>
<script src="js/bootstrap-formhelpers.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace('mailbody')

    $(function() {
        $("#mailsendButton").click( function()
                {
                    $("#mailform").submit()
                }
        );
    });


</script>
<%@ include file="footer.jsp" %>


</body>
</html>
