<%@ page import="nl.hu.tho4.Utils.BrandstofDAO" %>
<%@ page import="nl.hu.tho4.Utils.Utils" %>
<%@ page import="nl.hu.tho4.model.Brandstof" %>
<%@ page import="nl.hu.tho4.Utils.BrandstofDAO" %>

<% // Is ingelogd?
  if (session.getAttribute("username") == null) {
    response.sendRedirect("403.jsp");
  }

  // Check of er een parameter bij zit
  int brandstofID = 0;
  String rawBrandstofID = request.getParameter("bid");
  if (rawBrandstofID == null) {
    response.sendRedirect("500.jsp");
  } else {
    try {
      brandstofID = Integer.valueOf(rawBrandstofID);

    } catch (NumberFormatException e) {
      response.sendRedirect("500.jsp");
    }
  }
  Utils u = new Utils();
  BrandstofDAO bd = new BrandstofDAO();
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width-device-width, initial-scale=1.0">
  <title>ATD Auto Totaal Dienst</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/bootstrap-formhelpers.css" rel="stylesheet" media="screen">
  <link href="css/ATD.css" rel="stylesheet">
  <title>ATD Auto Totaal Dienst</title>
</head>
<body>
<%@ include file="header.jsp" %>
<div class="container">
  <div class = "page-header text-center">
    <h1>Overzicht Brandstof</h1>
  </div>
  <div class="col-md-3">
    <form method="POST" action="/setBrandstof.do">
      <input type="text" class="bfh-number form-control" name="hoeveelheid" value="<%out.print(bd.getStockById(brandstofID));%>">
      <input type="hidden" name="bid" value="<%out.print(brandstofID);%>">
      <input class="form-control btn-info btn-block" type="submit" value="Update Brandstof">
    </form>
  </div>
</div>


<%@ include file="footer.jsp" %>
<script src="js/jquery-2.1.3.min.js"></script>
<script src="js/bootstrap-formhelpers.js"></script>

</body>
</html>

