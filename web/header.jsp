<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1"/>
  <style type="text/css">
    label { float: left; width: 100px; }
  </style>
  <title>ATD Auto Totaal Dienst</title>
</head>
<body>
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">

        <a class="navbar-brand" href="/index.jsp">Auto Totaal Dienst
            <!--<img alt="Brand" src="band.png"> -->
        </a>
        <ul class="nav navbar-nav">
            <li><a href="/index.jsp">Home</a></li>
            <%
                if (session.getAttribute("role") != null) {
                   if (session.getAttribute("role").equals("Bedrijfsleider")){
                       out.print("<li><a href=\"/inplannen.jsp\">Nieuwe Klus</a></li>");
                       out.print("<li><a href=\"/viewKlussen.jsp\">Overzicht Klussen</a></li>");
                       out.print("<li><a href=\"/klantOverzicht.jsp\">Klanten</a></li>");
                       out.print("<li><a href=\"/brandstofOverzicht.jsp\">Brandstof</a></li>");
                       out.print("<li><a href=\"/magazijnOverzicht.jsp\">Magazijn</a></li>");
                       out.print("<li><a href=\"/parkeerBezetting.jsp\">Bezetting garage</a></li>");
                       out.print("<li><a href=\"/overzichtParkeerReserveringen.jsp\">Gereserveerde plaatsen</a></li>");
                       out.print("<li><a href=\"/tariefOverzicht.jsp\">Tarieven</a></li>");

                   }
                   if (session.getAttribute("role").equals("Monteur")){
                       out.print("<li><a href=\"/viewKlussen.jsp\">Overzicht Klussen</a></li>");
                       out.print("<li><a href=\"/klantOverzicht.jsp\">Klanten</a></li>");
                   }
                   if (session.getAttribute("role").equals("Parkeerbeheerder")) {
                       out.print("<li><a href=\"/overzichtParkeerReserveringen.jsp\">Gereserveerde plaatsen</a></li>");
                       out.print("<li><a href=\"/parkeerBezetting.jsp\">Bezetting garage</a></li>");
                   }
                } else {
                    out.print("<li><a href=\"/info.jsp\">Informatie</a></li>");
                    out.print("<li><a href=\"/klant/formulier.jsp\">Reparatie aanvragen</a></li>");
                    out.print("<li><a href=\"/klant/ParkeerplekReserveren.jsp\">Parkeerplek reserveren</a></li>");
                }
            %>


        </ul>
        <ul class="nav navbar-nav navbar-right">
            <%
                try {
                    if (session.getAttribute("username") != null) { // Blijkbaar kan dit voor een error zorgen als er net uitgelogd is
                        out.println("<p class=\"navbar-text\">Ingelogd als:" + session.getAttribute("username") + "</p>");
                        out.println("<li><a href=\"/logout.jsp\">Logout</a></li>");
                    } else {
                        out.println("<li><a href=\"/login.jsp\">Login</a></li>");
                    }
                } catch (Exception e) {
                    out.println("<li><a href=\"/login.jsp\">Login</a></li>");
                }

            %>

        </ul>
    </div>
</nav>
</body>