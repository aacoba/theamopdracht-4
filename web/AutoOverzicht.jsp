<%--
  Created by IntelliJ IDEA.
  User: Nelis
  Date: 27-5-2015
  Time: 13:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="nl.hu.tho4.Utils.AutoDAO" %>
<%@ page import="nl.hu.tho4.model.Klant" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="nl.hu.tho4.model.Auto" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% // Is ingelogd?
  if (session.getAttribute("username") == null) {
    response.sendRedirect("403.jsp");
  }
%>


<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width-device-width, initial-scale=1.0">
  <title>ATD Auto Totaal Dienst</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<%@ include file="header.jsp" %>
<div class="container">
  <div class = "page-header text-center">
    <h1>Klantenoverzicht</h1>
  </div>
  <div>
    <table class="table table-striped table-bordered table-responsive table-hover">
      <tr><th>KlantNummer</th><th>Kenteken</th><th>LaatsteAPK</th></tr>
      <%
        AutoDAO ad = new AutoDAO();

        ArrayList<Auto> list;

        list = ad.getAllCars();

        for (Auto a : list) {
          out.print("<tr>");
          out.print("<td>" + a.getKlantNr()+ "</td>");
          out.print("<td>" + a.getKenteken() + "</td>");
          out.print("<td>" + a.getLaatsteAPK() + "</td>");
          out.println("</tr>");
        }
      %>
    </table>
  </div>
</div>
<%@ include file="footer.jsp" %>
<script type="js/jquery-2.1.3.min.js"></script>
<script type="js/bootstrap.min.js"></script>
</body>
</html>

