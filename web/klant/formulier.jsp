<%@ page import="nl.hu.tho4.Utils.BrandstofDAO" %>
<%@ page import="nl.hu.tho4.Utils.Utils" %>
<%@ page import="nl.hu.tho4.model.Brandstof" %>
<%@ page import="nl.hu.tho4.Utils.BrandstofDAO" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width-device-width, initial-scale=1.0">
    <title>ATD Auto Totaal Dienst</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/bootstrap-formhelpers.css" rel="stylesheet" media="screen">
    <link href="../css/ATD.css" rel="stylesheet">
</head>
<body>
<%@ include file="/header.jsp" %>
<div class="container">
    <div class="page-header text-center">
        <h1>Afspraak aanvragen</h1>
    </div>
    <div class="col-md-offset-3">
        <div class="col-md-5">
            <form class="form-horizontal" action="sendRequest.do" method="POST">
                <div class="form-group">
                    <label for="KlantNaam">Naam</label>
                    <input type="text" class="form-control" id="KlantNaam" name="naam"
                           placeholder="Jan Janssen">
                </div>
                <div class="form-group">
                    <label for="KlantEmail">Email Adres</label>
                    <input type="email" class="form-control" id="KlantEmail" name="email"
                           placeholder="voorbeeld@voorbeeld.nl">
                </div>
                <div class="form-group">
                    <label for="KlantNummer">Telefoonnummer</label>
                    <input type="text" class="form-control" id="KlantNummer" name="nummer"
                           placeholder="06-12345678">
                </div>
                <div class="form-group">
                    <label for="KlantKenteken">Kenteken</label>
                    <input type="text" class="form-control" id="KlantKenteken" name="kenteken"
                           placeholder="AA-11-CC">
                </div>
                <div class="form-group">
                    <label for="KlantText">Waarvoor komt U?</label>
                    <textarea class="form-control" id="KlantText" name="text" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>


        </div>
    </div>
</div>


<%@ include file="/footer.jsp" %>
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap-formhelpers.js"></script>

</body>
</html>

