
<%@ page import="nl.hu.tho4.model.Parkeerprijs" %>
<%@ page import="nl.hu.tho4.Utils.ParkeerDAO" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width-device-width, initial-scale=1.0">
  <title>ATD Auto Totaal Dienst</title>
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link href="../css/bootstrap-formhelpers.css" rel="stylesheet" media="screen">
  <link href="../css/ATD.css" rel="stylesheet">
  <title>ATD Auto Totaal Dienst</title>
</head>
<body>
<%@ include file="/header.jsp" %>
<div class="container">
  <div class="page-header text-center">
    <h1>Parkeerplek Reserveren</h1>
  </div>
  <div class="col-md-offset-3">
    <div class="col-md-5">
      <form class="form-horizontal" action="sendRequest.do" method="POST">



        <div class="form-group">
          <label for="KlantKenteken">Kenteken</label>
          <input type="text" class="form-control" id="KlantKenteken" name="kenteken"
                 placeholder="AA-11-CC">
        </div>
        <div class="form-group">
          <label>Datum</label><br>
          <input class="form-control" type="date" name="parkeerDag">
        </div>
        <div class="form-group">
          <label>Periode</label>
          <select name="soort" class="form-control">
            <%
              ParkeerDAO pD = new ParkeerDAO();
              for (Parkeerprijs s: pD.getAllParkeertariefen()) {
                out.println("<option value=\"" +  "\">" + s.getSoort() + " " + s.getPrijs() + "Euro" + "</option>");
              }
            %>
          </select>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>


    </div>
  </div>
</div>


<%@ include file="/footer.jsp" %>
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap-formhelpers.js"></script>

</body>
</html>
