<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width-device-width, initial-scale=1.0">
	<title>ATD Auto Totaal Dienst</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/ATD.css" rel="stylesheet">

</head>
<body>
<%@ include file="header.jsp" %>

<div class="container">
	<div class = "jumbotron text-center">
		<%
			if (session.getAttribute("username") ==  null) {
				out.print("<h1>Welkom bij ATD!</h1>");
			} else {
				out.print("<h1>U bent ingelogd</h1>");
			}
		%>
		<p>Op deze interactieve site heeft u de mogelijkheid om alles te bekijken
			en te bewerken wat te maken heeft met de diensten van ATD, zoals reparaties
			inplannen/wijzigen, parkeerplekken reserveren, en factureren</p>
			<%
				if (session.getAttribute("username") ==  null) {
					out.println("<a class = \"btn-default btn\" href=\"login.jsp\">Login</a>");
				}
			%>
	</div>
</div>

<%@ include file="footer.jsp" %>

<script type="js/jquery-2.1.3.min.js"></script>
<script type="js/bootstrap.min.js"></script>

</body>
</html>