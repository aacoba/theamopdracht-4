* In alle servlets met "authenticatie" wordt de zelfde logica geimplementeerd. (Code duplication) Servlets/addGebruiktArtikel:23
* In alle DAOs  wordt een nieuwe verbinding gemaakt, wat opzichzelf niet een codesmell is, want DBDrivers zijn niet ThreadSafe, maar in elke DAO moesten de login gegevens worden meegegeven. (Utils/BrandstofDAO)
* Onduidelijke variabelnamen (KlusDAO: 193)
* Database verbindingen worden niet gesloten (Alle DAOs)
* Long method (Utils/MailUtils:63)

1 Login Filter toegevoegd, dit is de "way to do it" in JSP.  
2 In BaseDAO een getConnection() aangemaakt.   
3 Kleine refactor, gewoon "a" veranderd naar "artikel"  
4 In BaseDAO CloseStatement(Statement) toegevoegd en dit overal in de try catch finally toegevoegd aan de finally  
5 Dit is eigenlijk niet weg te refactoren zonder een templating engine, maar dit implementeren streeft het doel van de opdracht een beetje voorbij  
