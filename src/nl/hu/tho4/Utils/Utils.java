package nl.hu.tho4.Utils;

import java.text.NumberFormat;


public class Utils {

    /**
     * Format the given double in currency format
     * @param in Double value (eur, cents)
     * @return Formatted value
     */
    public String curFormat(double in) {
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
        return currencyFormatter.format(in);
    }

    public String formatUren(int in) {
        int min = in % 60;
        int hour = (in - min) / 60;
        return hour + ":" + String.format("%02d", min);
    }

    public String klusStatusSelector(String vName, String tName) {
        if (vName.equals(tName)) {
            return " selected ";
        } else {
            return "";
        }
    }

}
