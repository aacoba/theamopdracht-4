package nl.hu.tho4.Utils;

/**
 * Created by Nelis on 27-5-2015.
 */

import nl.hu.tho4.model.Brandstof;

import java.sql.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;

public class BrandstofDAO  extends BaseDAO  {
    public BrandstofDAO() {
        super();
    }

    private ArrayList<Brandstof> selectBrandstof(PreparedStatement stmt) {
        ArrayList<Brandstof> results = new ArrayList<>();


        try (Connection con = this.getConnection()) {


            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                int brandstofID  = rs.getInt(1);
                String naam = rs.getString(2);
                double gewensteHoeveelheid = rs.getDouble(3);
                double hoeveelheid = rs.getDouble(4);
                double prijsPerLiter = rs.getDouble(5);
                Brandstof newKraftstof = new Brandstof(brandstofID, naam, prijsPerLiter, hoeveelheid, gewensteHoeveelheid); // <-- I see what you did there and I like it!
                results.add(newKraftstof);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return results;
    }



    public ArrayList<Brandstof> getAllBrandstof() {
        try (Connection con = this.getConnection()) {
            PreparedStatement pst = con.prepareStatement("SELECT * FROM Brandstof WHERE 1");
            return selectBrandstof(pst);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public ArrayList<Brandstof> getThisBrandstof(int bID) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT * FROM Brandstof WHERE brandstofid = ?");
            pst.setInt(1, bID);
            return selectBrandstof(pst);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeStatement(pst);
        }
    }


    public void updateThisBrandstof(int bID, double hv) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("UPDATE Brandstof SET Hoeveelheid = ? WHERE BrandstofID = ?");
            pst.setDouble(1, hv);
            pst.setInt(2, bID);
            pst.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }
    }


    public ArrayList<Brandstof> getLowStock() {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT * FROM Brandstof WHERE `hoeveelheid` < `gewenstehoeveelheid`");
            return selectBrandstof(pst);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeStatement(pst);
        }

    }

    public int getStockById(int id) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT hoeveelheid FROM Brandstof WHERE brandstofid = ?");
            pst.setInt(1, id);
            pst.execute();
            ResultSet rs = pst.getResultSet();
            if (rs.next()) {
                return rs.getInt(1);
            } else {
                return -1;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            closeStatement(pst);
        }
    }

}
