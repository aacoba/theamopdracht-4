package nl.hu.tho4.Utils;

/**
 * Created by Nelis on 27-6-2015.
 */

import nl.hu.tho4.model.ParkeerBezetting;
import nl.hu.tho4.model.Parkeerprijs;
import nl.hu.tho4.model.Reservering;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ParkeerDAO  extends BaseDAO  {
    public ParkeerDAO() {
        super();
    }

    private ArrayList<Parkeerprijs> selectParkeerprijs(PreparedStatement pst) {
        ArrayList<Parkeerprijs> results = new ArrayList<>();


        ResultSet rs = null;
        try (Connection con = this.getConnection()) {
            rs = pst.executeQuery();

            while (rs.next()) {
                String soort = rs.getString(1);
                double prijs = rs.getDouble(2);
                int id = rs.getInt(3);
                Parkeerprijs newParkeerprijs = new Parkeerprijs(soort, prijs, id);
                results.add(newParkeerprijs);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }
        return results;
    }

    private ArrayList<ParkeerBezetting> selectBezetting(PreparedStatement pst) {
        ArrayList<ParkeerBezetting> results = new ArrayList<>();


        try (Connection con = this.getConnection()) {
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                String datum = rs.getString(1);
                int bezetting = rs.getInt(2);
                ParkeerBezetting newParkeerbezetting = new ParkeerBezetting(datum, bezetting);
                results.add(newParkeerbezetting);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }
        return results;
    }

    private ArrayList<Reservering> selectReservering(PreparedStatement pst){
        ArrayList<Reservering> results = new ArrayList<>();

        try (Connection con = this.getConnection()) {
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                int reserveringsID = rs.getInt(1);
                String datum = rs.getString(2);
                String kenteken = rs.getString(3);
                int soort = rs.getInt(4);
                Reservering newReservering = new Reservering(reserveringsID, datum, kenteken, soort);
                results.add(newReservering);
            }

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }
        return results;
    }

    public ArrayList<Reservering> getAllReservingen() {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT * FROM ParkeerReserveringen");
            return this.selectReservering(pst);


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeStatement(pst);
        }

    }

    public void addReservering(String datum, String kenteken, int soort) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("INSERT INTO ParkeerReserveringen (datum, kenteken, soort) VALUES (?, ?, ?)");
            pst.setString(1, datum);
            pst.setString(2, kenteken);
            pst.setInt(3, soort);
            pst.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }
    }

    public ArrayList<Parkeerprijs> getAllParkeertariefen() {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT * FROM ParkeerPrijs");
            return this.selectParkeerprijs(pst);


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeStatement(pst);
        }

    }

    public ArrayList<ParkeerBezetting> getAllBezetting() {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT * FROM ParkeerBezetting");
            return this.selectBezetting(pst);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeStatement(pst);
        }
    }

    public void updateParkeertarief(int id, double prijs) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("UPDATE `THO4`.`ParkeerPrijs` SET `prijs` = ? WHERE `ParkeerPrijs`.`soortid` = ?;");
            pst.setDouble(1, prijs);
            pst.setInt(2, id);
            pst.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }

    }

    public void addBezetting(String datum, int bezetting) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("INSERT INTO `THO4`.`ParkeerBezetting` (`datum`, `aantalBezet`) VALUES (?, ?);");
            pst.setString(1, datum);
            pst.setInt(2, bezetting);
            pst.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }
    }

    public boolean bezettingExists(String datum) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT * FROM `ParkeerBezetting` WHERE `datum` = ?");
            pst.setString(1, datum);
            ResultSet rs = pst.executeQuery();
            int rowcount = 0;
            if (rs.last()) {
                rowcount = rs.getRow();
                rs.beforeFirst();
            }

            return rowcount > 0;


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }
        return true;
    }

    public void updateBezetting(String datum, int bezetting) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("UPDATE `THO4`.`ParkeerBezetting` SET `aantalBezet` = ? WHERE `ParkeerBezetting`.`datum` = ?;");
            pst.setString(2, datum);
            pst.setInt(1, bezetting);
            pst.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }
    }

    public double getPrijsByID(int id) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT * FROM ParkeerPrijs WHERE soortid = ?");
            pst.setInt(1, id);
            return this.selectParkeerprijs(pst).get(0).getPrijs();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }
        return 0;
    }

    public String getNaamById(int id) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT * FROM ParkeerPrijs WHERE soortid = ?");
            pst.setInt(1, id);
            return this.selectParkeerprijs(pst).get(0).getSoort();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }
        return null;
    }


}
