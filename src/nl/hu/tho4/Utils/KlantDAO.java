package nl.hu.tho4.Utils;

/**
 * Created by Nelis on 27-5-2015.
 */
import nl.hu.tho4.model.Klant;

import javax.lang.model.type.ExecutableType;
import java.sql.*;
import java.util.ArrayList;

public class KlantDAO  extends BaseDAO  {
    public KlantDAO() {
        super();
    }

    private ArrayList<Klant> selectKlanten(PreparedStatement pst) {
        ArrayList<Klant> results = new ArrayList<>();


        try (Connection con = this.getConnection()) {
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                int klantNr = rs.getInt(1);
                String initiaalen = rs.getString(2);
                String naam = rs.getString(3);
                String adres = rs.getString(4);
                String postcode = rs.getString(5);
                String email = rs.getString(6);
                Date laatsteHerinnering = rs.getDate(7);
                Klant newCustomer = new Klant(klantNr, naam, initiaalen, adres, postcode, email, laatsteHerinnering);
                results.add(newCustomer);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }
        return results;
    }

    public ArrayList<Klant> getAllKlanten() {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT * FROM Klant");
            return this.selectKlanten(pst);


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeStatement(pst);
        }

    }

    public ArrayList<Klant> getLangNietGeweest() {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT * FROM Klant WHERE klantnr IN (SELECT Klant_klantnr FROM Auto WHERE kenteken NOT IN (SELECT Auto_kenteken FROM Klus) OR (Select Auto_kenteken FROM Klus WHERE datum < ? ))");
            pst.setString(1, "2014-1-31");
            return this.selectKlanten(pst);


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeStatement(pst);
        }

    }

    public ArrayList<Klant> getVerlopenAPK() {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT * FROM Klant WHERE klantnr IN (SELECT Klant_klantnr FROM Auto WHERE laatsteAPK < ?)");
            pst.setString(1, "2014-1-31");
            return this.selectKlanten(pst);


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeStatement(pst);
        }
    }

    public Klant getKlantByID(int id) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT * FROM Klant WHERE klantnr = ?");
            pst.setInt(1, id);
            ArrayList<Klant> rst = selectKlanten(pst);
            if (rst.isEmpty()) {
                return null;
            } else {
                return rst.get(0);
            }


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeStatement(pst);
        }
    }

    public Klant getKlantByKlusID(int id) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT * FROM Klant WHERE klantnr IN (SELECT Klant_klantnr FROM Auto WHERE kenteken IN (SELECT Auto_kenteken FROM Klus WHERE klusnr = ? ))");
            pst.setInt(1, id);
            ArrayList<Klant> rst = selectKlanten(pst);
            if (rst.isEmpty()) {
                return null;
            } else {
                return rst.get(0);
            }


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeStatement(pst);
        }
    }

    public ArrayList<Klant> searchKlant(String q) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT * FROM `Klant` WHERE `klantnr` LIKE ? OR `initiaalen` LIKE ? OR `naam` LIKE ? OR `adres` LIKE ? OR `postcode` LIKE ? OR `email` LIKE ?");
            pst.setString(1, q);
            pst.setString(2, "%" + q + "%");
            pst.setString(3, "%" + q + "%");
            pst.setString(4, "%" + q + "%");
            pst.setString(5, q);
            pst.setString(6, "%" + q + "%");
            return this.selectKlanten(pst);


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeStatement(pst);
        }
    }

    public void updateKlant(int id, String initialen, String naam, String adres, String postcode, String email) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("UPDATE `THO4`.`Klant` SET `initiaalen` = ?, naam = ?, adres = ?, postcode = ?, email = ? WHERE `Klant`.`klantnr` = ?;");
            pst.setString(1, initialen);
            pst.setString(2, naam);
            pst.setString(3, adres);
            pst.setString(4, postcode);
            pst.setString(5, email);
            pst.setInt(6, id);
            pst.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }
    }
}