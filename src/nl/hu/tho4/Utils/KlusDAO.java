package nl.hu.tho4.Utils;

import nl.hu.tho4.model.Artikel;
import nl.hu.tho4.model.Klus;
import nl.hu.tho4.model.Monteur;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class KlusDAO  extends BaseDAO  {
    public KlusDAO() {
        super();
    }

    private List<Klus> selectKlussen(PreparedStatement stmt) {
        List<Klus> results = new ArrayList<>();


        try (Connection con = this.getConnection()) {
            MonteurDAO montDAO = new MonteurDAO();
            //Statement stmt = con.createStatement();

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                int klusnr = rs.getInt(1);
                Timestamp datum = rs.getTimestamp("datum");
                Monteur mont = montDAO.getMonteurById(rs.getInt("Monteur_monteurid"));
                String status = rs.getString("status");
                int gewerkteTijd = rs.getInt("gewerkteTijd");
                String kenteken = rs.getString("Auto_kenteken");
                String beschrijving = rs.getString("beschrijving");

                Klus newKlus = new Klus(klusnr, datum, mont, status, kenteken, gewerkteTijd, beschrijving);
                ArtikelDAO ad = new ArtikelDAO();
                for (Artikel a : ad.getGebruikteArtikelenByKlusId(klusnr)) {
                    newKlus.addGebruiktArtikel(a);
                }
                results.add(newKlus);
            }



        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(stmt);
        }
        return results;
    }

    public ArrayList<Artikel> getGebruikteArtikelen(int KlusID) {
        try {
            ArtikelDAO artDAO = new ArtikelDAO();
            return artDAO.getGebruikteArtikelenByKlusId(KlusID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public void addGebruiktArtikel(int kid, int ean) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            String q = "INSERT INTO `THO4`.`GebruikteArtikelen` (`gebrArtId`, `Klus_klusnr`, `Artikel_EAN`) VALUES (NULL, ?, ?)";
            pst = con.prepareStatement(q);
            pst.setInt(1, kid);
            pst.setInt(2, ean);
            pst.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }
    }

    public void setStatus(int klusId, String status) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            String q = "UPDATE `Klus` SET status=? WHERE `klusnr` = ?";
            pst = con.prepareStatement(q);
            pst.setString(1, status);
            pst.setInt(2, klusId);
            pst.execute();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }
        // UPDATE `Klus` SET `klusnr`=[value-1],`datum`=[value-2],`monteurid`=[value-3],`status`=[value-4],`gewerkteTijd`=[value-5] WHERE 1
        //String bq = "UPDATE Klus SET status="

    }

    public void setTijd(int klusId, int tijd) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            String q = "UPDATE `Klus` SET gewerkteTijd=? WHERE `klusnr` = ?";
            pst = con.prepareStatement(q);
            pst.setInt(1, tijd);
            pst.setInt(2, klusId);
            pst.execute();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }

    }
    public void setKorting(int klusId, int kP) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            String q = "UPDATE `Klus` SET korting=? WHERE `klusnr` = ?";
            pst = con.prepareStatement(q);
            pst.setInt(1, kP);
            pst.setInt(2, klusId);
            pst.execute();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }

    }

    public ArrayList<Klus> getAllKlussen() {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT * FROM Klus");
            return (ArrayList<Klus>) this.selectKlussen(pst);


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeStatement(pst);
        }
    }

    public int getWerktijdById(int id) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT gewerkteTijd FROM Klus WHERE klusnr = ?");
            pst.setInt(1, id);
            pst.execute();
            ResultSet rs = pst.getResultSet();
            if (rs.next()) {
                return rs.getInt(1);
            } else {
                return -1;
            }


        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            closeStatement(pst);
        }
    }

    public int getKortingById(int id) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT korting FROM Klus WHERE klusnr = ?");
            pst.setInt(1, id);
            pst.execute();
            ResultSet rs = pst.getResultSet();
            if (rs.next()) {
                return rs.getInt(1);
            } else {
                return -1;
            }


        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            closeStatement(pst);
        }
    }

    public Klus getKlusById(int id) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT *  FROM Klus WHERE klusnr = ?");
            pst.setInt(1, id);
            ArrayList<Klus> klussen = (ArrayList<Klus>) this.selectKlussen(pst);
            if (klussen.isEmpty()) {
                return null;
            } else {
                return klussen.get(0);
            }


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeStatement(pst);
        }
    }

    public double getArtikelenPrijsById(int id)     {
        double result = 0;
        for (Artikel artikel : this.getGebruikteArtikelen(id)) {
            result += artikel.getAdviesPrijs();
        }
        return result;
    }
    public double getTotaal(int id){
        double totaal = getArtikelenPrijsById(id) + ((getWerktijdById(id) / 60) * 25.00);

        return totaal;
    }
    public double getTotaalPrijs(int id){
        double totaalPrijs = getTotaal(id) * (100-getKortingById(id)) / 100;

        return totaalPrijs;
    }


    public String getStatus(int id) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT status FROM Klus WHERE klusnr = ?");
            pst.setInt(1, id);
            pst.execute();
            ResultSet rs = pst.getResultSet();
            if (rs.next()) {
                return rs.getString(1);
            } else {
                return null;
            }


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeStatement(pst);
        }
    }

    public boolean addReparatie(String dt, int Monteurid, String kenteken, String desc) {

        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            String q = "INSERT INTO `THO4`.`Klus` (`klusnr`, `datum`, `status`, `gewerkteTijd`, `Monteur_monteurid`, `Auto_kenteken`, `beschrijving`) VALUES (NULL, ?, 'In Wacht', '0', ?, ?, ? );";
            pst = con.prepareStatement(q);
            pst.setString(1, dt);
            pst.setInt(2, Monteurid);
            pst.setString(3, kenteken);
            pst.setString(4, desc);
            if (pst.executeUpdate() < 1) {
                return false;
            } else {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }
        return false;

    }

    public void updateBeschrijving(int klusId, String desc) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            String q = "UPDATE `Klus` SET beschrijving=? WHERE `klusnr` = ?";
            pst = con.prepareStatement(q);
            pst.setString(1, desc);
            pst.setInt(2, klusId);
            pst.execute();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }
    }



    public ArrayList<Klus> getKlussenByKlant(int klantId) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT * FROM Klus WHERE Auto_kenteken IN (SELECT kenteken FROM `Auto` WHERE Klant_klantnr = ?)");
            pst.setInt(1, klantId);
            return (ArrayList<Klus>) this.selectKlussen(pst);


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeStatement(pst);
        }
    }

    public String getHTMLTable(ArrayList<Klus> input) {
        StringBuilder sb = new StringBuilder();
        sb.append("<table class=\"table table-striped table-bordered table-responsive table-hover\">\n" +
                "      <tr><th>ID</th><th>Datum</th><th>Auto</th><th>Status</th><th>Gewerkte Minuten</th><th>Monteur</th><th>Info</th></tr>");
        for (Klus k : input) {
            sb.append("<tr>");
            sb.append("<td>" + k.getKlusNr() + "</td>");
            sb.append("<td>" + k.getNormalDate() + "</td>");
            sb.append("<td>" + k.getKenteken() + "</td>"); // TODO Auto DAO
            sb.append("<td>" + k.getStatus() + "</td>");
            sb.append("<td>" + k.getGewerkteTijd() + "</td>");
            sb.append("<td>" + k.getMonteur().getVoornaam() + " " + k.getMonteur().getAchternaam() + "</td>");
            sb.append("<td><a href=\"klus.jsp?kid=" + k.getKlusNr() + "\"class=\"btn btn-info btn-block\">Info</a></td>");
            sb.append("</tr>\n");
        }
        sb.append("</table>");
        return sb.toString();
    }




}
