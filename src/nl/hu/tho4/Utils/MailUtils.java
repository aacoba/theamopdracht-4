package nl.hu.tho4.Utils;

import nl.hu.tho4.model.Artikel;
import nl.hu.tho4.model.Klant;
import nl.hu.tho4.model.Klus;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Calendar;
import java.util.Properties;


public class MailUtils {
    protected static final String username = "atd@mailserver.aacoba.net";
    protected static final String password = "3T2ZvqXhs5";
    protected static final String hostname = "delta.aacoba.net";
    protected static final int port = 587;
    protected static final boolean checkSSL = false;
    protected static final boolean ssl = false;
    protected static final String from = "atd@mailserver.aacoba.net";


    public boolean sendMail(String to, String subject, String body) {

        Properties props = new Properties();
        props.put("mail.smtp.host", hostname);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.ssl.enable", ssl);
        props.put("mail.smtp.starttls.enable", true);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.ssl.checkserveridentity", "false");
        props.put("mail.smtp.ssl.trust", "*");
        Session mailSession = Session.getInstance(props);
        try {
            MimeMessage msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(from, "Autotaaldiensten"));
            msg.setRecipients(Message.RecipientType.TO, to);
            msg.setSubject(subject);
            msg.setSentDate(Calendar.getInstance().getTime());
            msg.setContent(body, "text/html");

            Transport.send(msg, username, password);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        System.out.println("Email verstuurd naar :" + to);
        return true;
    }

    public String getHerrinneringBody(Klant k) {
        String r = "Beste " + k.getInitiaalen() + " " + k.getNaam() + "<br>" +
                "Dit is een herinneringsbrief<br>" +
                "Hier moet iets komen te staan<br>" +
                "<br><br>" +
                "Met Vriendelijke Groet, <br>Autotaaldiensten";
        return r;
    }

    public String getFactuurBody(Klus k) {
        try {

            Utils u = new Utils();
            KlusDAO kd = new KlusDAO();
            StringBuilder sb = new StringBuilder();
            sb.append("<!DOCTYPE html>\n" +
                    "<html>\n" +
                    "<head>\n" +
                    "    <link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css\" rel=\"stylesheet\">\n" +
                    "\n" +
                    "\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "    <h1>Dit is een factuur!</h1>\n" +
                    "    <h1>Gebruikte Artikelen</h1>\n" +
                    "    <table class=\"table table-striped table-bordered table-responsive table-hover\">\n" +
                    "        <tr>\n" +
                    "            <th>Naam</th>\n" +
                    "            <th>Naam</th>\n" +
                    "            <th>Prijs</th>\n" +
                    "        </tr>\n");
                        for (Artikel a : kd.getGebruikteArtikelen(k.getKlusNr())) {
                            sb.append("<tr>");
                            sb.append("<td>" + a.getNaam() + "</td>");
                            sb.append("<td>" + a.getEuropeesArtikelNummer() + "</td>");
                            sb.append("<td>" + a.getAdviesPrijsEuro() + "</td>");
                            sb.append("</tr>");
                        }
            sb.append(
                    "    </table>\n" +
                    "\n" +
                    "    <div class=\"col-md-5\">\n" +
                    "        <br><br><br><table class=\n" +
                    "        \"table table-striped table-bordered table-responsive table-hover\">\n" +
                    "            <tr>\n" +
                    "                <td>Aantal Werkuren</td>\n" +
                    "                <td>" + u.formatUren(kd.getWerktijdById(k.getKlusNr())) + "</td>\n" +
                    "            </tr>\n" +
                    "            <tr>\n" +
                    "                <td>Totaal</td>\n" +
                    "                <td>" + u.curFormat(kd.getTotaal(k.getKlusNr())) + "</td>\n" +
                    "            </tr>\n" +
                    "            <tr>\n" +
                    "                <td>Korting</td>\n" +
                    "                <td>"+ kd.getKortingById(k.getKlusNr()) + "</td>\n" +
                    "            </tr>\n" +
                    "            <tr>\n" +
                    "                <td>Subtotaal</td>\n" +
                    "                <td>" + u.curFormat(kd.getTotaalPrijs(k.getKlusNr())) + "</td>\n" +
                    "            </tr>\n" +
                    "        </table>\n" +
                            "" +
                            "<br><br><h3>Mvg. Autotaaldiensten" +
                    "    </div><script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js\"></script>\n" +
                    "</body>\n" +
                    "</html>");


            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "ERROR";

    }

}
