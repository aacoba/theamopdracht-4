package nl.hu.tho4.Utils;

import java.sql.*;

/**
 * Base Data Access Object with the basic MySQL connection parameters TEST
 * @author Auke Bakker
 */
public class BaseDAO {
    private static final String DB_URL = "jdbc:mysql://aacoba.net/THO4";
    private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_USERNAME = "THO4";
    private static final String DB_PASSWORD = "N2ww3ZVMbKfYnZLa";

    public BaseDAO(){
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Coult not find Database Driver!");
            System.out.println("ABORTING!");
            System.exit(-1);
        }
    }


    /**
     * Function to test the connection and request the version of the MySQL Database server
     * @author Auke Bakker
     * @return MySQL Database server Version
     */
    public String getVersion() {
        Statement stmt = null;
        ResultSet rs = null;
        try (Connection con = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD)) {
            stmt = con.createStatement();

            // create SQL query
            String query = "SELECT VERSION()";
            rs = stmt.executeQuery(query);
            rs.next();
            return rs.getString("VERSION()");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(stmt);
            closeResultset(rs);
        }
        return "Error in BaseDAO";
    }



    protected Connection getConnection() throws SQLException {
        return DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
    }

    public void closeStatement(Statement pst) {
        if (pst != null) {
            try {
                pst.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    void closeResultset(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}


