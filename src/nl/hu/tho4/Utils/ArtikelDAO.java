package nl.hu.tho4.Utils;


import nl.hu.tho4.model.Artikel;

import java.sql.*;
import java.util.ArrayList;

public class ArtikelDAO  extends BaseDAO  {
    public ArtikelDAO(){
        super();
    }

    private ArrayList<Artikel> selectArtikelen(PreparedStatement pst) {
        ArrayList<Artikel> results = new ArrayList<>();


        try (Connection con = this.getConnection()) {
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                String naam = rs.getString(1);
                int hoeveelheid = rs.getInt(2);
                int EAN = rs.getInt(3);
                int Gewenstehoeveelheid = rs.getInt(4);
                double inkoopprijs = rs.getDouble(5);
                double adviesprijs = rs.getDouble(6);

                Artikel newArtikel = new Artikel(naam, hoeveelheid, EAN, Gewenstehoeveelheid, inkoopprijs, adviesprijs);
                results.add(newArtikel);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return results;
    }


    public Artikel getArtikelByEAN(int EAN) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT * FROM Artikel WHERE EAN = ?");
            pst.setInt(1, EAN);

            ArrayList<Artikel> rst = this.selectArtikelen(pst);

            if (rst.isEmpty()) {
                return null;
            } else return rst.get(0);


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeStatement(pst);
        }
    }

    public ArrayList<Artikel> getGebruikteArtikelenByKlusId(int KlusID) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT * FROM Artikel INNER JOIN GebruikteArtikelen ON GebruikteArtikelen.Artikel_EAN=Artikel.EAN WHERE GebruikteArtikelen.Klus_klusnr = ?");
            pst.setInt(1, KlusID);
            return this.selectArtikelen(pst);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeStatement(pst);
        }
    }

    public ArrayList<Artikel> getAllArtikelen() {
        try (Connection con = this.getConnection()) {
            PreparedStatement pst = con.prepareStatement("SELECT * FROM Artikel");
            return this.selectArtikelen(pst);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public ArrayList<Artikel> searchArtikelen(String q) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT * FROM `Artikel` WHERE `naam` LIKE ? OR `EAN` = ?");
            pst.setString(1, "%" + q + "%");
            pst.setString(2, q);
            return this.selectArtikelen(pst);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeStatement(pst);
        }

    }

    public ArrayList<Artikel> getLowStock() {
        try (Connection con = this.getConnection()) {
            PreparedStatement pst = con.prepareStatement("SELECT * FROM `Artikel` WHERE `hoeveelheid` < `Gewenstehoeveelheid`");
            return this.selectArtikelen(pst);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }



}

