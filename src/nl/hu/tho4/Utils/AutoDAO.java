package nl.hu.tho4.Utils;

/**
 * Created by Nelis on 27-5-2015.
 */
import nl.hu.tho4.model.Auto;

import java.sql.*;
import java.util.ArrayList;

public class AutoDAO  extends BaseDAO {
    public AutoDAO() {
        super();
    }

    private ArrayList<Auto> selectCars(PreparedStatement pst) {
        ArrayList<Auto> results = new ArrayList<>();


        try (Connection con = this.getConnection()) {
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                String kenteken = rs.getString(1);
                int klantNr = rs.getInt(2);
                String laatsteAPK = rs.getString(3);
                Auto newAuto = new Auto(kenteken, klantNr,laatsteAPK);
                results.add(newAuto);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }
        return results;
    }

    public ArrayList<Auto> getAllCars() {
        try (Connection con = this.getConnection()) {
            PreparedStatement pst = con.prepareStatement("SELECT * FROM Auto");
            return this.selectCars(pst);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public ArrayList<Auto> getCustomerCarsByID(int id) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT * FROM Auto WHERE Klant_klantnr = ?");
            pst.setInt(1, id);
            return this.selectCars(pst);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            closeStatement(pst);
        }
    }
}