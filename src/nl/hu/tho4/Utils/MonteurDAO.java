package nl.hu.tho4.Utils;

import nl.hu.tho4.model.Monteur;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class MonteurDAO extends BaseDAO{

    public MonteurDAO() {
        super();
    }

    private List<Monteur> selectKlussen(String query) {
        List<Monteur> results = new ArrayList<>();

        Statement stmt = null;
        ResultSet rs = null;
        try (Connection con = this.getConnection()) {
            stmt = con.createStatement();

            rs = stmt.executeQuery(query);
            while (rs.next()) {
                int id = rs.getInt(1);
                String voornaam = rs.getString(2);
                String achternaam = rs.getString(3);
                Monteur newMonteur = new Monteur(id, voornaam, achternaam);
                results.add(newMonteur);
            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeStatement(stmt);
            closeResultset(rs);

        }
        return results;
    }

    public Monteur getMonteurById(int id) {
        String bq = "SELECT * FROM Monteur WHERE monteurid = ";
        bq += String.valueOf(id);

        ArrayList<Monteur> rst = (ArrayList<Monteur>) this.selectKlussen(bq);
        if (rst.isEmpty()) {
            return null;
        } else return rst.get(0);
    }

    public ArrayList<Monteur> getAllMonteurs() {
        String bq = "SELECT * FROM Monteur WHERE 1";
        return (ArrayList<Monteur>) this.selectKlussen(bq);
    }
}
