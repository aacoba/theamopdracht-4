package nl.hu.tho4.model;

/**
 * Created by Nelis on 27-5-2015.
 */
public class Brandstof {
    private String naam;
    private double gewensteHoeveelheid;
    private double hoeveelheid;
    private double prijsPerLiter;
    private int brandstofID;

    public Brandstof(int bid, String nm, double ppl, double hv, double ghv){
        brandstofID = bid;
        naam = nm;
        prijsPerLiter = ppl;
        hoeveelheid = hv;
        gewensteHoeveelheid = ghv;
    }

    public int getBrandstofID(){
        return brandstofID;
    }

    public String getNaam(){
        return naam;
    }

    public double getPrijsPerLiter(){
        return prijsPerLiter;
    }

    public double getHoeveelheid(){
        return hoeveelheid;
    }

    public double getGewensteHoeveelheid(){
        return gewensteHoeveelheid;
    }
}
