package nl.hu.tho4.model;

import nl.hu.tho4.Utils.MailUtils;

import java.sql.Date;


public class Klant {
    private String naam;
    private String initiaalen;
    private String adres;
    private String postcode;
    private String email;
    private int klantnr;
    private Date laatsteHerinnering;


    public Klant(int nr, String nm, String ini, String ad, String ps, String em, Date lh){
        klantnr = nr;
        naam = nm;
        initiaalen = ini;
        adres = ad;
        postcode = ps;
        email = em;
        laatsteHerinnering = lh;

    }

    public Date getLaatsteHerinnering() {
        return laatsteHerinnering;
    }

    public String getEmail() {
        return email;
    }

    public int getKlantnr(){
        return klantnr;
    }

    public String getNaam(){
        return naam;
    }

    public String getInitiaalen(){
        return initiaalen;
    }

    public String getAdres(){
        return adres;
    }

    public String getPostcode(){
        return postcode;
    }

    public void sendEmail(String subject, String body) {
        new MailUtils().sendMail(email, subject, body);
    }

}
