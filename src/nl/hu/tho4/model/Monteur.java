package nl.hu.tho4.model;

public class Monteur {
	int id;
	private String voornaam;
	private String achternaam;
	
	public Monteur(int i, String vnm, String anm){
		id = i;
		voornaam = vnm;
		achternaam = anm;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getVoornaam() {
		return voornaam;
	}

	public void setVoornaam(String voornaam) {
		this.voornaam = voornaam;
	}

	public String getAchternaam() {
		return achternaam;
	}

	public void setAchternaam(String achternaam) {
		this.achternaam = achternaam;
	}
}
