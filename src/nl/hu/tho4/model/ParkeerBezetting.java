package nl.hu.tho4.model;

/**
 * Created by Auke on 24-6-2015.
 */
public class ParkeerBezetting {
    private String datum;
    private int bezetting;

    public ParkeerBezetting(String datum, int bezetting) {
        this.datum = datum;
        this.bezetting = bezetting;
    }

    public String getDatum() {
        return datum;
    }

    public int getBezetting() {
        return bezetting;
    }
}
