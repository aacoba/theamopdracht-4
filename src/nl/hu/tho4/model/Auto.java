package nl.hu.tho4.model;
/**
 * Created by Nelis on 24-6-2015.
 */
public class Auto {
    private String kenteken;
    private int klantNr;
    private String laatsteAPK;

    public Auto(String ke, int kn, String va){
        kenteken = ke;
        klantNr = kn;
        laatsteAPK = va;
    }

    public String getKenteken(){
        return kenteken;
    }

    public int getKlantNr(){
        return klantNr;
    }

    public String getLaatsteAPK(){
        return laatsteAPK;
    }
}
