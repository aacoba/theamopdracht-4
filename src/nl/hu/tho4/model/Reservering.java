package nl.hu.tho4.model;

/**
 * Created by Gebruiker on 24-6-2015.
 */
public class Reservering {
    private int reserveringsID;
    private String datum;
    private String kenteken;
    private int soort;

    public Reservering(int rs, String dt, String kt, int st){
        reserveringsID = rs;
        datum = dt;
        kenteken = kt;
        soort = st;
    }

    public int getReserveringsID(){
        return reserveringsID;
    }

    public String getDatum(){
        return datum;
    }

    public String getKenteken(){
        return kenteken;
    }
    public int getSoort(){return soort;}

}
