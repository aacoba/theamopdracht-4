package nl.hu.tho4.model;

import java.text.NumberFormat;

public class Artikel {
	private String naam;
	private int hoeveelheid;
	private int productNr;
	private boolean beschikbaar;
	private int europeesArtikelNummer;
	private int gewensteHoeveelheid;
	private double inkoopPrijs;
	private double adviesPrijs;
	
	public Artikel(String nm, int hvh, int ean, int ghvh, double ikp, double advp){
		naam = nm;
		hoeveelheid = hvh;
		europeesArtikelNummer = ean;
		gewensteHoeveelheid = ghvh;
		inkoopPrijs = ikp;
		adviesPrijs = advp;
	}

	public String getNaam() {
		return naam;
	}

	public int getHoeveelheid() {
		return hoeveelheid;
	}

	public int getProductNr() {
		return productNr;
	}

	public boolean isBeschikbaar() {
		return beschikbaar;
	}

	public int getEuropeesArtikelNummer() {
		return europeesArtikelNummer;
	}

	public int getGewensteHoeveelheid() {
		return gewensteHoeveelheid;
	}

	public double getInkoopPrijs() {
		return inkoopPrijs;
	}

	public double getAdviesPrijs() {
		return adviesPrijs;
	}

	public String getAdviesPrijsEuro() {
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
		return currencyFormatter.format(this.adviesPrijs);
	}
}
