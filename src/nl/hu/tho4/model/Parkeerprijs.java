package nl.hu.tho4.model;

/**
 * Created by Nelis on 24-6-2015.
 */
public class Parkeerprijs {
    private int soortid;
    private String soort;
    private double prijs;

    public Parkeerprijs(String st, double ps, int sid){
        soort = st;
        prijs = ps;
        soortid = sid;
    }

    public String getSoort(){
        return soort;
    }

    public double getPrijs(){
        return prijs;
    }

    public int getSoortid(){ return soortid;}

    public void setSoort(String st){
        soort= st;
    }

    public void setSoortid(int sid){ soortid = sid;}

    public void setPrijs(double ps){
        prijs = ps;
    }
}
