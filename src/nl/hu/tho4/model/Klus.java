package nl.hu.tho4.model;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Klus {
	private int klusNr;
	private Timestamp datum;
	private Monteur monteur;
	private String status;
	private int gewerkteTijd;
	private ArrayList<Artikel> gebruikteArtikelen;
	private String kenteken;
	private String beschrijving;

	public Klus(int nr, Timestamp dt, Monteur m, String status, String kenteken, int gewerkteTijd, String desc) {
		gebruikteArtikelen = new ArrayList<>();
		this.klusNr = nr;
		this.datum = dt;
		this.monteur = m;
		this.status = status;
		this.gewerkteTijd = gewerkteTijd;
		this.kenteken = kenteken;
		this.beschrijving = desc;
	}


	public String getKenteken() {
		return kenteken;
	}

	public String getBeschrijving() {
		return beschrijving;
	}

	public int getKlusNr() {
		return klusNr;
	}

	public Timestamp getDatum() {
		return datum;
	}

	public Monteur getMonteur() {
		return monteur;
	}

	public String getStatus() {
		return status;
	}

	public int getGewerkteTijd() {
		return gewerkteTijd;
	}

	public List<Artikel> getArtikelen() {
		return gebruikteArtikelen;
	}

	public String getNormalDate() {
		return this.datum.toLocalDateTime().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"));
	}


	public void addGebruiktArtikel(Artikel a) {
		this.gebruikteArtikelen.add(a);
	}
}
