package nl.hu.tho4.Servlets;

import nl.hu.tho4.Utils.BrandstofDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Auke on 4-6-2015.
 */
public class setBrandstof extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            HttpSession ses = req.getSession();

            if (ses.getAttribute("username") == null) { // Is ingelogd?
                resp.sendRedirect("403.jsp");
            }



            String rawBrandstofId = req.getParameter("bid");
            String rawHoeveelheid = req.getParameter("hoeveelheid");

            int brandstofId = -1;
            double hoeveelheid = -1;

            try {
                brandstofId = Integer.parseInt(rawBrandstofId);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                resp.sendRedirect("/500.jsp");
            }

            try {
                hoeveelheid = Double.parseDouble(rawHoeveelheid);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                resp.sendRedirect("/500.jsp");
            }

            BrandstofDAO bd = new BrandstofDAO();

            bd.updateThisBrandstof(brandstofId, hoeveelheid);

            resp.sendRedirect("/brandstofOverzicht.jsp");

        } catch (Exception e) {
            e.printStackTrace();
        }



    }
}
