package nl.hu.tho4.Servlets;

import nl.hu.tho4.Utils.AutoDAO;
import nl.hu.tho4.model.Auto;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Auke on 25-6-2015.
 */
public class getKlantAutos extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();

        HttpSession ses = req.getSession();
        if (ses.getAttribute("username") == null) { // Is ingelogd?
            resp.sendRedirect("403.jsp");
        }

        String rawKlantId = req.getParameter("kid");

        int kid = -1;

        try {
            kid = Integer.valueOf(rawKlantId);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            resp.sendRedirect("/500.jsp");
        }
        try {
            AutoDAO ad = new AutoDAO();

            for (Auto a: ad.getCustomerCarsByID(kid)) {
                out.println(a.getKenteken());
            }


        } catch (Exception e) {
            e.printStackTrace();
        }



    }
}
