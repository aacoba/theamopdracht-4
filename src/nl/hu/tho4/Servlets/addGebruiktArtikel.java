package nl.hu.tho4.Servlets;

import nl.hu.tho4.Utils.KlusDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * Created by Auke on 4-6-2015.
 */
public class addGebruiktArtikel extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            HttpSession ses = req.getSession();

            if (ses.getAttribute("username") == null) { // Is ingelogd?
                resp.sendRedirect("403.jsp");
            }



            String rawEan = req.getParameter("ean");
            String rawKid = req.getParameter("klusid");

            int ean = -1;
            int kid = -1;

            try {
                ean = Integer.valueOf(rawEan);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                resp.sendRedirect("/500.jsp");
            }

            try {
               kid = Integer.valueOf(rawKid);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                resp.sendRedirect("/500.jsp");
            }

            KlusDAO kd = new KlusDAO();

            kd.addGebruiktArtikel(kid, ean);

            resp.sendRedirect("/klus.jsp?kid="+kid);


        } catch (Exception e) {
            e.printStackTrace();
        }



    }
}
