package nl.hu.tho4.Servlets;

import nl.hu.tho4.Utils.ParkeerDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Auke on 24-6-2015.
 */
public class BezettingServlet extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("username") == null) {
            resp.sendRedirect("/403.jsp");
        }
        String rawDate = req.getParameter("datum");
        String rawBezetting = req.getParameter("bezetting");


        int bezetting = -1;

        try {
            bezetting = Integer.valueOf(rawBezetting);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            resp.sendRedirect("/500.jsp");
        }
        try {
            ParkeerDAO pd = new ParkeerDAO();
            if (pd.bezettingExists(rawDate)) {
                pd.updateBezetting(rawDate, bezetting);
            } else {
                pd.addBezetting(rawDate, bezetting);
            }
            resp.sendRedirect("/parkeerBezetting.jsp");
        } catch (Exception e) {
            e.printStackTrace();
            resp.sendRedirect("/500.jsp");
        }




    }
}
