package nl.hu.tho4.Servlets;

import nl.hu.tho4.Utils.ParkeerDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by AvW on 25-6-2015.
 */
public class parkeerReserveringServlet extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String kenteken = req.getParameter("kenteken");
        String datum = req.getParameter("parkeerDag");
        String rawSoort = req.getParameter("soort");

        int soort = -1;
        try {
            soort = Integer.valueOf(rawSoort);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            resp.sendRedirect("/500.jsp");
        }

        try {
            ParkeerDAO pd = new ParkeerDAO();
            pd.addReservering(datum, kenteken, soort);
            resp.sendRedirect("/index.jsp");
        } catch (Exception e) {
            e.printStackTrace();
            resp.sendRedirect("/500.jsp");
        }

    }
}
