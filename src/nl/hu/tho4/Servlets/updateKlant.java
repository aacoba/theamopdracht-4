package nl.hu.tho4.Servlets;

import nl.hu.tho4.Utils.KlantDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class updateKlant extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            HttpSession ses = req.getSession();

            if (ses.getAttribute("username") == null) { // Is ingelogd?
                resp.sendRedirect("403.jsp");
            }


            String rawKlantId = req.getParameter("klantNr");


            int klantId = -1;



            try {
                klantId = Integer.valueOf(rawKlantId);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                resp.sendRedirect("/500.jsp");
            }

            KlantDAO kd = new KlantDAO();


            String initialen = req.getParameter("initialen");
            String naam = req.getParameter("naam");
            String adres = req.getParameter("adres");
            String postcode = req.getParameter("postcode");
            String email = req.getParameter("email");

            kd.updateKlant(klantId, initialen, naam, adres, postcode, email);

            resp.sendRedirect("/klant.jsp?kid=" + klantId);

        } catch (Exception e) {
            e.printStackTrace();
        }



    }
}
