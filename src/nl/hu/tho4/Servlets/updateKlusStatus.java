package nl.hu.tho4.Servlets;

import nl.hu.tho4.Utils.KlusDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class updateKlusStatus extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            HttpSession ses = req.getSession();

            if (ses.getAttribute("username") == null) { // Is ingelogd?
                resp.sendRedirect("403.jsp");
            }

            String rawKlusid = req.getParameter("klusID");
            String rawStatus = req.getParameter("status");

            int klusID = -1;

            try {
                klusID = Integer.valueOf(rawKlusid);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                resp.sendRedirect("/500.jsp");
            }

            KlusDAO kd = new KlusDAO();


            switch (rawStatus) {
                case "In Wacht":
                    kd.setStatus(klusID, "In Wacht");
                    break;
                case "Begonnen":
                    kd.setStatus(klusID, "Begonnen");
                    break;
                case "Klaar":
                    kd.setStatus(klusID, "Klaar");
                    break;
                case "Betaald":
                    kd.setStatus(klusID, "Betaald");
                    break;
                default:
                    resp.sendRedirect("/500.jsp");
            }

            resp.sendRedirect("/klus.jsp?kid=" + klusID);

        } catch (Exception e) {
            e.printStackTrace();
        }



    }
}
