package nl.hu.tho4.Servlets;

import nl.hu.tho4.Utils.MailUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class sendMail extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            HttpSession ses = req.getSession();

            if (ses.getAttribute("username") == null) { // Is ingelogd?
                resp.sendRedirect("403.jsp");
                return;
            }

            String to = req.getParameter("to");
            String subject = req.getParameter("subject");
            String body = req.getParameter("body");
            String redirect = req.getParameter("nextRedir");

            MailUtils mu = new MailUtils();
            mu.sendMail(to, subject, body);
            resp.sendRedirect(redirect);
        } catch (Exception e) {
            e.printStackTrace();
        }






    }
}
