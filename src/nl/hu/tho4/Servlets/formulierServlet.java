package nl.hu.tho4.Servlets;

import nl.hu.tho4.Utils.MailUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Auke on 24-6-2015.
 */
public class formulierServlet extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String rawEmail = req.getParameter("email");
        String rawNaam = req.getParameter("naam");
        String rawKenteken = req.getParameter("kenteken");
        String rawText = req.getParameter("text");
        String rawNummer = req.getParameter("nummer");

        MailUtils mu = new MailUtils();
        StringBuilder sb = new StringBuilder();
        sb.append("<html>" +
                "<body>" +
                "   <p>Nieuw inplan verzoek van : ");
        sb.append(rawNaam);
        sb.append("</p></br>" +
                "<p>Email: " + rawEmail);
        sb.append("<br>Telefoonnummer: " + rawNummer);
        sb.append("<br>Kenteken: " + rawKenteken);
        sb.append("<br>Rede voor bezoek:<br>" + rawText);

        mu.sendMail("atd@mailserver.aacoba.net", "Nieuw afspraakverzoek van " + rawNaam, sb.toString());
        resp.sendRedirect("/index.jsp");



    }
}
