package nl.hu.tho4.Servlets;

import nl.hu.tho4.Utils.ParkeerDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Auke on 25-6-2015.
 */
public class setPrijs extends HttpServlet {

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String rawId = req.getParameter("id");
        String rawPrijs = req.getParameter("prijs");

        int id = -1;

        try {
            id = Integer.valueOf(rawId);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            resp.sendRedirect("/500.jsp");
        }
        double prijs = -1;

        try {
            prijs = Double.valueOf(rawPrijs);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            resp.sendRedirect("/500.jsp");
        }

        try {
            ParkeerDAO pd = new ParkeerDAO();
            pd.updateParkeertarief(id, prijs);
            resp.sendRedirect("/tariefOverzicht.jsp");
        } catch (Exception e) {
            e.printStackTrace();
            resp.sendRedirect("/500.jsp");
        }


    }
}
