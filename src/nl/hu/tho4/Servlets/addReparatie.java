package nl.hu.tho4.Servlets;

import nl.hu.tho4.Utils.KlusDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Auke on 21-5-2015.
 */
public class addReparatie extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            HttpSession ses = req.getSession();

            if (ses.getAttribute("username") == null) { // Is ingelogd?
                resp.sendRedirect("403.jsp");
            }


            KlusDAO kDAO = new KlusDAO();

            String rawMonteur = req.getParameter("monteur");
            String rawAuto = req.getParameter("auto");
            String rawDag = req.getParameter("reparatiedag");
            String rawTijd = req.getParameter("reparatietijd");
            String rawBeschrijving = req.getParameter("desc");


            String[] dateParts = rawDag.split("-");
            int year = Integer.parseInt(dateParts[0]);
            int month = Integer.parseInt(dateParts[1]);
            int day = Integer.parseInt(dateParts[2]);

            String[] timeParts = rawTijd.split(":");
            int hour = Integer.parseInt(timeParts[0]);
            int minutes = Integer.parseInt(timeParts[1]);

           String timestamp = year + "-" + month + "-" + day + " " + hour + ":" + minutes;


            int montID = Integer.parseInt(rawMonteur);
            kDAO.addReparatie(timestamp, montID, rawAuto, rawBeschrijving);
            resp.sendRedirect("/viewKlussen.jsp");


        } catch (Exception e) {
            e.printStackTrace();
            resp.sendRedirect("/500.jsp");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("/index.jsp");
    }
}
