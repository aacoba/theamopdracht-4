package nl.hu.tho4.LoginUtils;

import nl.hu.tho4.Utils.BaseDAO;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.servlet.http.HttpSession;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.sql.*;



public class LoginUtils extends BaseDAO{
    public LoginUtils() throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        super();
    }


    /**
     * Encrypts the password
     * @author Auke Bakker
     * @version 1.0
     * @param password The password that needs to be encrytped with the supplied salt
     * @param salt The salt to use for encryption
     * @return Byte array of the resulting hash
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public byte[] getEncryptedPassword(String password, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException{
        String algorithm = "PBKDF2WithHmacSHA1";
        int derivedKeyLength = 160;
        int iterations = 20000;

        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, derivedKeyLength);

        SecretKeyFactory f = SecretKeyFactory.getInstance(algorithm);
        return f.generateSecret(spec).getEncoded();

    }


    /**
     * Generates a secure random salt for password encryption
     * @author Auke Bakker
     * @version 1.0
     * @return byte array of the salt
     * @throws NoSuchAlgorithmException
     */
    public byte[] generateSalt() throws NoSuchAlgorithmException {
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");

        byte[] salt = new byte[8];
        random.nextBytes(salt);

        return salt;
    }

    /**
     *
     * @param att_password The password validate
     * @param encryptedPassword  encrypted password hash from the persistence interface
     * @param salt salt used in encryption
     * @return true if the password is valid false if the password is incorrect
     */
    public boolean checkPass(String att_password, String encryptedPassword, String salt){
        try {
            byte[] encryptedAtteptedPassword = getEncryptedPassword(att_password, salt.getBytes());
//            System.out.println(new String(encryptedAtteptedPassword));
//            System.out.println(new String(encryptedPassword));

            return (new String(encryptedAtteptedPassword).equals(encryptedPassword));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Get the users role
     * @author Auke Bakker
     * @param username Username to lookup
     * @return Role as String
     */
    public String getRole(String username) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT role FROM userlogin WHERE username = ?");
            pst.setString(1, username);
            pst.execute();

            ResultSet rs = pst.getResultSet();
            rs.next();
            return rs.getString("role");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }
        return null;
    }


    /**
     * User login function
     * @param username The username
     * @param password The password
     * @param session The HTTPSession
     * @return did the login succeed?
     */
    public boolean login(String username, String password, HttpSession session) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            pst = con.prepareStatement("SELECT * FROM userlogin WHERE username = ?");
            pst.setString(1, username);
            pst.execute();

            ResultSet rs = pst.getResultSet();
            int rowcount = 0;
            if (rs.last()) {
                rowcount = rs.getRow();
                rs.beforeFirst();
            }
            switch (rowcount) {
                case 1:
                    // Er is ��n login gevonden
                    rs.next();
                    String passhash = rs.getString("passhash");
                    String passsalt = rs.getString("passsalt");

                    if (this.checkPass(password, passhash, passsalt)) {
                        session.setAttribute("username", username);
                        session.setAttribute("role", this.getRole(username));
                        return true;
                    } else {
                        return false;
                    }
                default:
                    return false;
                // Er zijn te veel of geen results FOUT!
            }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeStatement(pst);
        }
        return false;
    }

    /**
     * Add the user (does not check the password etc)
     * @param username Username
     * @param password Raw password
     * @param role User role
     * @param email Email address
     * @return true if the user was added successfully false if it failed
     */
    public boolean addUser(String username, String password, String role, String email) {
        PreparedStatement pst = null;
        try (Connection con = this.getConnection()) {
            try {
                byte[] salt = generateSalt();
                byte[] passHash = getEncryptedPassword(password, salt);
                pst = con.prepareStatement("INSERT INTO `THO4`.`userlogin` (`id`, `username`, `passhash`, `passsalt`, `email`, `role`) VALUES (NULL, ?, ?, ?, ?, ?);");
                pst.setString(1, username);
                pst.setString(2, new String(passHash));
                pst.setString(3, new String(salt));
                pst.setString(4, email);
                pst.setString(5, role);
                pst.execute();
                return pst.getMaxRows() >= 1;


            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                return false;
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
            }


        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeStatement(pst);
        }
        return false;
    }
}
