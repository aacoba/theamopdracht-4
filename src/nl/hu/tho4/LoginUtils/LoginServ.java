package nl.hu.tho4.LoginUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginServ extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {


            String reqUsername = req.getParameter("username");
            String reqPassword = req.getParameter("password");

            if (!"".equals(reqUsername) || !"".equals(reqPassword)) {
                LoginUtils lu = new LoginUtils();
                if (lu.login(reqUsername, reqPassword, req.getSession())) {
                    resp.sendRedirect("/index.jsp");
                } else {
                    resp.sendRedirect("/login.jsp?msg=invallid");

                    System.out.println("Login fout!");

                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            resp.sendRedirect("/500.jsp");
            System.out.println("Exception in loginServlet");
        }



    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("login.jsp");
    }
}
